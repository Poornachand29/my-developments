import json
import unittest
import lambda_function
from unittest.mock import patch, Mock 

with open('mockresponse.txt') as f:
    mockdata = json.load(f)

with open('response.txt') as f:
    data = json.load(f)

class Validateresponse(unittest.TestCase):
    @patch('lambda_function.boto3')
    def test_config_response(self, MockConfig):
        instance = MockConfig()
        instance.fetch_resource_info.return_value = mockdata
        response = instance.fetch_resource_info()
        self.assertEqual(response,data) 

if __name__ == '__main__':
    unittest.main()


