import base64
import unittest
import datetime
import json
import random
import pytest
import botocore.session
from src.lambda_function import fetch_resource_info
from src.lambda_function import write_to_kinesis
from botocore.stub import Stubber

#target = __import__("main")
#config_agg = target.get_agg_res_conf
aggregator_name = "Inventory-Account-Aggregator"
config = botocore.session.get_session().create_client('config', region_name = "us-east-1")
kinesis = botocore.session.get_session().create_client('kinesis', region_name = "us-east-1")

with open('response.txt') as f:
        data = json.load(f)
class Validateresponse(unittest.TestCase):
   
    def test_config_response(self):
        stubber = Stubber(config)
        expected_params = {'ConfigurationAggregatorName' : aggregator_name,
                           'ResourceIdentifier' : {
                           'SourceAccountId': '327246106564',
                           'SourceRegion': 'us-east-1',
                           'ResourceId': 'i-000d686500a2b7497',
                           'ResourceType': 'AWS::EC2::Instance' }}
        stubber.add_response('get_aggregate_resource_config', data, expected_params)
        #stubber.activate()
        resource = { 'SourceAccountId': '327246106564',
                     'SourceRegion': 'us-east-1',
                     'ResourceId': 'i-000d686500a2b7497'
                   }
        resource_type = 'AWS::EC2::Instance'
        with stubber:
                result = fetch_resource_info(resource_type,resource)
        service_response = result['ConfigurationItem']['configuration']
        input_response = data['ConfigurationItem']['configuration']
        put_records = self.stub_put_records(result)
        self.assertEqual(input_response , service_response)

    def test_config_client_error(self):
        stubber = Stubber(config)
        expectedparams = {'ConfigurationAggregatorName' : aggregator_name,
                           'ResourceIdentifier' : {
                           'SourceAccountId': '327246106564',
                           'SourceRegion': 'us-east-1',
                           'ResourceId': 'i-000d686500a2b7494',
                           'ResourceType': 'AWS::EC2::Instance' }}
        stubber.add_client_error('get_aggregate_resource_config', service_error_code='ResourceNotDiscoveredException', service_message='resourceid not found in the aggregator', http_status_code=404, expected_params=expectedparams)
        stubber.activate()
        response = config.get_aggregate_resource_config(ConfigurationAggregatorName=aggregator_name,
                                                                ResourceIdentifier={
                                                                'SourceAccountId': '327246106564',
                                                                'SourceRegion': 'us-east-1',
                                                                'ResourceId': 'i-000d686500a2b7494',
                                                                'ResourceType':'AWS::EC2::Instance' })
        print(response)

    def stub_put_records(self, result):
        service_response = write_to_kinesis(result)             

if __name__ == '__main__':
    unittest.main()

