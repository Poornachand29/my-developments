import json
import boto3
import botocore
import botocore.exceptions
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from awsretry import AWSRetry
import os
import datetime
import time 
import lambda_function

aggregator_name = os.environ['AGGREGATOR_NAME']

client  = boto3.client('config')


def fetch_volume_info(ec2_resource_information):
    volume_list = []
    volume_list_db = []
    ec2_detailed_information = json.loads(ec2_resource_information["configuration"])
    print("ec2_detailed_information",ec2_detailed_information)
    root_device_info = ec2_detailed_information["rootDeviceName"]
    print("root_device_info",root_device_info)
    for volume_data in ec2_detailed_information["blockDeviceMappings"]:
        volume_info,volume_db = fetch_volume_status(volume_data,root_device_info)
        volume_list.append(volume_info)
        volume_list_db.append(volume_db)
    return volume_list,volume_list_db

def fetch_volume_status(volume_data,root_device_info):
    deviceName = volume_data["deviceName"]
    volumeId = volume_data['ebs']["volumeId"]
    resource_type = 'AWS::EC2::Volume'
    list_volumes = lambda_function.aggregate_discovered_resources(resource_type)
    volume_resource = list_volumes['ResourceIdentifiers']
    volume_structure = {}
    while True:
        for volume_info in volume_resource:
            if ( volume_info['ResourceId'] ==  volumeId ):
                fetch_volume,volume_structure,volume_structure_db = fetch_volume_detailed_info(volume_info,volumeId,root_device_info)
                if fetch_volume["ResponseMetadata"]["HTTPStatusCode"] == 200:
                    print ("Successfully fetched volume_info " + volume_info['ResourceId'])
                    break
                else:
                    print ("failed to fetch volume_info" + volume_info['ResourceId'])

        if 'NextToken' in list_volumes:
            list_volumes = lambda_function.aggregate_discovered_resources_next_token(resource_type,list_volumes['NextToken'])
            volume_resource = list_volumes['ResourceIdentifiers']
        else:
            break
        
    empty_dict = not volume_structure
    if ( str(empty_dict) == "True"):
        volume_structure_dict = { "volumeId" : { 'S': volumeId } }
        volume_structure = { "M" : volume_structure_dict }
        volume_structure_db = { "volumeId" : volumeId }
        return volume_structure,volume_structure_db
    else:
        return volume_structure,volume_structure_db
    
@AWSRetry.backoff(tries=20, delay=2, backoff=1.5, added_exceptions=['Rate exceeded'])    	
def fetch_volume_detailed_info(volume_info,volumeId, root_device_info):
    try:
        volume_response = client.get_aggregate_resource_config(
            ConfigurationAggregatorName=aggregator_name,
            ResourceIdentifier={
                'SourceAccountId': volume_info['SourceAccountId'],
                'SourceRegion': volume_info['SourceRegion'],
                'ResourceId': volumeId,
                'ResourceType': 'AWS::EC2::Volume'
                }
        )
    except ClientError as error:
        print(error)
        raise error
      
    #print(volume_response)
    volume_resource_information = volume_response['ConfigurationItem']
    volume_resource_detailed_information = json.loads(volume_resource_information['configuration'])
    deviceName = volume_resource_detailed_information['attachments'][0]['device']
    if ( root_device_info == deviceName ):
        rootdevicestatus = 'true'
    else:
        rootdevicestatus = 'false'
    volumeId = volume_resource_detailed_information['attachments'][0]['volumeId']
    encrypted = volume_resource_detailed_information['encrypted']
    volumeType = volume_resource_detailed_information['volumeType']
    iops = volume_resource_detailed_information['iops']
    volumesize = volume_resource_detailed_information['size']
    multiAttachEnabled = volume_resource_detailed_information['multiAttachEnabled']
    tags = str(volume_resource_detailed_information['tags'])
    volume_structure = {
        "deviceName": {'S': deviceName},
        "rootdevicestatus": {'S': str(rootdevicestatus)},
        "volumeId": {'S': volumeId},
        "encrypted": {'S': str(encrypted)},
        "volumeType": {'S': volumeType},
        "volumesize": {'S': str(volumesize)},
        "iops": {'S': str(iops)},
        "multiAttachEnabled":{'S': str(multiAttachEnabled)},
        "tags": {'S': tags},
    }
    volume_structure_db = {
        "deviceName": deviceName,
        "rootdevicestatus": str(rootdevicestatus),
        "volumeId": volumeId,
        "encrypted": str(encrypted),
        "volumeType": volumeType,
        "volumesize":  str(volumesize),
        "iops":  str(iops),
        "multiAttachEnabled": str(multiAttachEnabled),
        "tags":  tags,
    } 
    volume_structure_dict = { "M" : volume_structure }
    return volume_response,volume_structure_dict,volume_structure_db







