import json
import boto3
import botocore
import botocore.exceptions
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
import os
import datetime
import time 
dynamodb = boto3.client('dynamodb')
table_name = os.environ['TABLE_NAME']
stream_table_name = os.environ['STREAM_TABLE_NAME']
stream_dynamodbr = boto3.resource('dynamodb')
stream_table = stream_dynamodbr.Table(stream_table_name)
import lambda_function
primary_key = 'ResourceId'
tag_name = "tag_name"
tag_value = "tag_value"

def UpdateDB(ec2_detailed_information,volume_data_dict,volume_data_dict_db,reg_record):
    ec2_detailed_information_load = json.loads(ec2_detailed_information['configuration'])
    networkInterface_list,sg_list,networkInterface_db,sg_db = lambda_function.fetch_ni_list(ec2_detailed_information_load)
    relationships = lambda_function.fetch_relationships(ec2_detailed_information)
    compulsory_tags,man_tags_db = lambda_function.fetch_mandatory_tags(ec2_detailed_information)
    non_compulsory_tags,non_man_tags_db = lambda_function.fetch_non_mandatory_tags(ec2_detailed_information)
    current_time = lambda_function.get_time()
    res_creation_time = str(ec2_detailed_information["resourceCreationTime"])
    res_creation_time_formated = res_creation_time[:10] + 'T' + res_creation_time[11:19] + 'Z'
    item={
                "accountid"         				: { "S": ec2_detailed_information["accountId"]           }, 
                "region"                            : { "S": ec2_detailed_information["awsRegion"]           }, 
                "ResourceId"        	     		: { "S": ec2_detailed_information["resourceId"]          }, 
                "resourceType"      				: { "S": ec2_detailed_information["resourceType"]        }, 
                "resourceCreationTime"              : { "S": res_creation_time_formated} ,
                "lastmodifieddate"                  : { "S": current_time},
                "current_status"                    : { "S": ec2_detailed_information_load["state"]["name"]},
                "ImageId"            				: { "S": ec2_detailed_information_load["imageId"]},
                "arn"   							: { "S": ec2_detailed_information["arn"]     }, 
                "availabilityZone"					: { "S": ec2_detailed_information["availabilityZone"] }, 
                "instanceType"   				    : { "S": ec2_detailed_information_load["instanceType"]     },
                "privateIpAddress"   				: { "S": ec2_detailed_information_load["privateIpAddress"]     },
                "privateDnsName"                    : { "S": ec2_detailed_information_load["privateDnsName"]     },
                "vpcId"   				 			: { "S": ec2_detailed_information_load["vpcId"]     },
                "subnetId"   				 		: { "S": ec2_detailed_information_load["subnetId"]},
                "NetworkInterface"   				: { "L": networkInterface_list     },
                "Volumes"							: { "L": volume_data_dict },
                "relationships"                     : { "L": relationships },
                "securitygroups"                    : { "L": sg_list},
                "mandatory_tags"                    : { "L":  compulsory_tags},
                "non_mandatory_tags"                : { "L":  non_compulsory_tags},
                "created_by_event"                  : { "S": "false"}
        }
    for tag in compulsory_tags:
        man_tag = { tag["M"][tag_name]["S"] : tag["M"][tag_value] }
        item.update(man_tag)
    item_region = item['region']['S']
    try:
        record_list = reg_record[item_region]['Items']
        resource_id_list = []
        for resid in record_list:
            resource_id_list.append(resid[primary_key])
    except KeyError:
        resource_id_list = []
    if item['ResourceId']['S'] in resource_id_list:
        for record in record_list:
            if item['ResourceId']['S'] == record[primary_key]:
                #print("Comparings items for ",record[primary_key])
                item_keys = item.keys()
                db_keys = record.keys()
                for item_key in item_keys:
                    if item_key in db_keys:
                        if  item_key == 'NetworkInterface':
                            net_sort_db = sorted(networkInterface_db)
                            net_sort = sorted(record[item_key])
                            if net_sort_db == net_sort:
                                continue
                            else:
                                continue
                                putitem = put_item(item)
                                return putitem
                        elif item_key == 'securitygroups' :
                            sg_sort_db = sorted(sg_db)
                            sg_sort = sorted(record[item_key])
                            if sg_sort == sg_sort_db:
                                continue
                            else:
                                continue
                                putitem = put_item(item)
                                return putitem
                        elif item_key == 'Volumes' :
                            try:
                                vol_sort_db = sorted(volume_data_dict_db, key = lambda i: i['deviceName'])
                                vol_sort = sorted(record[item_key], key = lambda i: i['deviceName'])
                                if vol_sort == vol_sort_db:
                                    continue
                                else:
                                    continue
                                    putitem = put_item(item)
                                    return putitem
                            except Exception as e:
                                print(f"Volume data key is missing {item[primary_key]['S']}")
                                print(e)
                                if  str(e) == 'KeyError':
                                    putitem = put_item(item) 
                                    return putitem
                        elif item_key == 'mandatory_tags' :
                            man_tag_sort_db = man_tags_db
                            man_tag_sort = record[item_key]
                            man_sort_db = sorted(man_tag_sort_db, key = lambda i: i[tag_name])
                            man_sort = sorted(man_tag_sort, key = lambda i: i[tag_name])
                            if man_sort_db == man_sort:
                                continue
                            else:
                                continue
                                putitem = put_item(item)
                                return putitem
                        elif item_key == 'non_mandatory_tags':
                            non_man_tag_sort_db = non_man_tags_db
                            non_man_tag_sort = record[item_key]
                            non_man_sort_db = sorted(non_man_tag_sort_db, key = lambda i: i[tag_name])
                            non_man_sort = sorted(non_man_tag_sort, key = lambda i: i[tag_name])
                            if non_man_sort_db == non_man_sort:
                                continue
                            else:
                                continue
                                putitem = put_item(item)
                                return putitem
                        elif item_key == 'relationships' :
                            continue 
                        elif item_key == 'resourceCreationTime' :
                            continue 
                        elif item_key == 'lastmodifieddate' :
                            continue
                        elif item_key == 'created_by_event' :
                            continue
                        else:
                            if item[item_key]['S'] == record[item_key] :
                                continue
                            else:
                                print("Mismatch for key: ",record[item_key])
                                putitem = put_item(item)
                                return putitem
                    else:
                        print("Updating the table for the key missing in " , item[primary_key]['S'])
                        putitem = put_item(item)
                        return putitem
    else:
        print("New Resource found updating the table for " , item[primary_key]['S'])
        putitem = put_item(item)
        return putitem
            
def put_item(item):
    try:
        response =  stream_table.query(
            KeyConditionExpression=Key(primary_key).eq(item[primary_key]['S'])
        )
        response_list = response['Items']
        status_list = []
        for res in response_list:
            status_list.append(res['current_status'])
        if 'terminated' in status_list:
            item_terminated = True
        else:
            item_terminated = False

        if  item_terminated:
            print("Not updating deleted item, item to be removed from aggregator",item[primary_key]['S'])
        else: 
            print("Updating record for resource ",item[primary_key]['S'])
            Update_Table = dynamodb.put_item(
                TableName=table_name,
                    Item=item
            )
            return Update_Table
    except ClientError as e:
        print("Updating record for resource ",item[primary_key]['S'])
        Update_Table = dynamodb.put_item(
            TableName=table_name,
                Item=item
        )
        return Update_Table
