import os
import json
import boto3
import botocore
import botocore.exceptions
from awsretry import AWSRetry
import datetime

# datetime object containing current date and time
now = datetime.datetime.now()
print("now =", now)
dt_string = now.strftime("%H:%M:%S")
#Importing environmental variables
masteraccount=os.environ['Master_account_ID']
sns_topic_arn = os.environ['SNS_TOPIC_ARN']
mandatorytags = os.environ['MANDATORY_TAGS']
#nonmandatorytags = os.environ['NON_MANDATORY_TAGS']
mandatory_tags_list = mandatorytags.split(',')
#non_mandatory_tags_list = nonmandatorytags.split(',')
master_stack = 'SC-' + masteraccount
#connecting clients
rolearn = os.environ['MASTER_ACCOUNT_ROLE']
sts_connection = boto3.client('sts')
acct_b = sts_connection.assume_role(
        RoleArn=rolearn,
        RoleSessionName="cross_acct_lambda"
    )
ACCESS_KEY = acct_b['Credentials']['AccessKeyId']
SECRET_KEY = acct_b['Credentials']['SecretAccessKey']
SESSION_TOKEN = acct_b['Credentials']['SessionToken']

cloudformation_client = boto3.client('cloudformation',aws_access_key_id=ACCESS_KEY,aws_secret_access_key=SECRET_KEY,aws_session_token=SESSION_TOKEN,)
organization_client = boto3.client('organizations',aws_access_key_id=ACCESS_KEY,aws_secret_access_key=SECRET_KEY,aws_session_token=SESSION_TOKEN,)
sns_client = boto3.client('sns')

tenam = datetime.datetime.strptime('10:00:00', '%H:%M:%S').strftime("%H:%M:%S")
elevenam = datetime.datetime.strptime('11:00:00', '%H:%M:%S').strftime("%H:%M:%S")


@AWSRetry.backoff(tries=20, delay=2, backoff=1.5, added_exceptions=['Rate exceeded'])
def lambda_handler(event, context):
    #List stacks by the following filter
    list_stacks = cloudformation_client.list_stacks(
        StackStatusFilter=[
            'CREATE_COMPLETE','UPDATE_IN_PROGRESS','UPDATE_COMPLETE_CLEANUP_IN_PROGRESS','UPDATE_COMPLETE'
        ]
    )
    stack_names = list_stacks['StackSummaries']


    #Get the list of accounts
    paginator = organization_client.get_paginator("list_accounts")
    pages = paginator.paginate()
    
    accounts_list = []
    for page in pages:
        if len(page["Accounts"]) != 0:
            for accountid in page['Accounts']:
                if accountid['Status'] == 'ACTIVE':
                    accounts_list.append(accountid['Id'])
    print("accounts list:",accounts_list)
    Headers = "AccountId, Stackname , Missing tags" + "\n"
    cf_account_list = []
    tags_account_msg = Headers

    
    more_stacks = True
    while more_stacks:
        for stack in list_stacks.get("StackSummaries"):
            stack_name = stack['StackName']
            if stack_name.startswith(master_stack):
                fetch_stack_info = cloudformation_client.describe_stacks(StackName=stack_name)
                print("stack info: ",fetch_stack_info)
                tags_account_msg,cf_account_list = distribute_tags(fetch_stack_info,accounts_list,tags_account_msg,cf_account_list)
                if fetch_stack_info["ResponseMetadata"]["HTTPStatusCode"] == 200:
                    print ("Successfully fetched account_info " )
                else:
                    print ("failed to fetch account_info" )
        next_token = list_stacks.get("NextToken")
        if next_token:
            list_stacks = cloudformation_client.list_stacks(
                NextToken=next_token,
                StackStatusFilter=[
                    'CREATE_COMPLETE','UPDATE_IN_PROGRESS','UPDATE_COMPLETE_CLEANUP_IN_PROGRESS','UPDATE_COMPLETE'
                    ]   
            )
        else:
            more_stacks = False
    
    #print("tags_account_msg",tags_account_msg)
    #print("list of accounts having cloud formation stack",cf_account_list)
    for acc in accounts_list:
        #if the account does not have corresponding cf stack
        if acc not in cf_account_list:
                #check the account is present or not
                paginator = organization_client.get_paginator("list_tags_for_resource")
                pages = paginator.paginate(ResourceId=acc)

                #Get the keys of tags in account
                key_list = []
                for page in pages:
                    if len(page["Tags"]) != 0:
                        for key in page['Tags']:
                            key_list.append(key['Key'])
     
                print(f"Keys present in account missing cf stack {acc} {key_list}")
                
                missing_keys = []
                for key in mandatory_tags_list:
                    if key not in key_list:
                        missing_keys.append(key)
                    
                if len(missing_keys) != 0:
                    row = [acc , "NA"] 
                    tags_string = "#".join(missing_keys)
                    row.append(tags_string)
                    new_line = ",".join(row) + "\n"
                    if new_line[-1] != "\n":
                        new_line = new_line + "\n"
                        
                    tags_account_msg = tags_account_msg + new_line
    
    if tenam <= dt_string <= elevenam:
        sub = "Mandatory tags are missing in the accounts"        
        publish_message(tags_account_msg,sub)
    
    
def distribute_tags(fetch_stack_info,accounts_list,tags_account_msg,cf_account_list):
    stack_name = fetch_stack_info['Stacks'][0]['StackName']
    tags = fetch_stack_info['Stacks'][0]['Tags']
    mandatory_tags = {}
    nonmandatory_tags = {}
    
    #fetch account id
    outputs  = fetch_stack_info['Stacks'][0]['Outputs']
    for output in outputs:
        if output['OutputKey'] == 'AccountID':
            accountid_value = output['OutputValue']
            
    #assigning tag values available in stack info
    for tag in tags:
        if tag['Key'] == 'appid' or tag['Key'] == 'application_id' or tag['Key'] == 'citisysteminventory':
            mandatory_tags['application_id'] = tag['Value']
        elif tag['Key'] == 'CMP_Order_ID' or tag['Key'] == 'request_id':
            mandatory_tags['request_id'] = tag['Value']
        elif tag['Key'] == 'Global_Organizational_Code' or tag['Key'] == 'Global_Organizational_code':
            mandatory_tags['sectorspecificgoc'] = tag['Value']
        elif tag['Key'] == 'Region' or tag['Key'] == 'region':
            nonmandatory_tags['CMP_regions'] = tag['Value']
        elif tag['Key'] == 'cart_req_soeid':
            mandatory_tags['cart_req_soeid'] = tag['Value']
        elif tag['Key'] == 'cartid' or tag['Key'] == 'architecture_review_id' :
            mandatory_tags['architecture_review_id'] = tag['Value']
        elif tag['Key'] == 'cloudops' or tag['Key'] == 'cloud_support_group':
            mandatory_tags['cloud_support_group'] = tag['Value']
        elif tag['Key'] == 'appinstid' or tag['Key'] == 'app_inst_id' :
            mandatory_tags['app_inst_id'] = tag['Value']
        elif tag['Key'] == 'chargeback_profile_id' or tag['Key'] == 'billingprofileid':
            mandatory_tags['chargeback_profile_id'] = tag['Value']
        elif tag['Key'] == 'secondary_chargeback_profile_id' or tag['Key'] == 'secondarybillingprofileid':
            mandatory_tags['secondary_chargeback_profile_id'] = tag['Value']
        elif tag['Key'] == 'sector'  or tag['Key'] == 'Sector':
            nonmandatory_tags['sector'] = tag['Value']
        elif tag['Key'] == 'environment' or tag['Key'] == 'workload_Environment' or tag['Key'] == 'Workload_Environment':
            mandatory_tags['environment'] = tag['Value']
        elif "aws:servicecatalog:" in tag['Key']:
            continue
        elif tag['Key'] == 'organization_unit':
            continue
        else:
            nonmandatory_tags[tag['Key']] = tag['Value']

    
    #Get OU Id and Name stack info
    ou_id_and_name = get_parent(accountid_value)

    mandatory_tags['organization_unit'] =  ou_id_and_name['ou_Id']


    #assigning parameter values avaiable in stack info as tags
    for parameter in fetch_stack_info['Stacks'][0]['Parameters']:
        if parameter['ParameterKey'] == 'OrgUnitName' :
            mandatory_tags['account_type'] = parameter['ParameterValue']
        elif parameter['ParameterKey'] == 'VPCRegion':
            nonmandatory_tags['CMP_VPCregion'] = parameter['ParameterValue']
        elif parameter['ParameterKey'] == 'VPCCidr':
            nonmandatory_tags['CMP_VPCCidr'] = parameter['ParameterValue']
        elif parameter['ParameterKey'] == 'PeerVPC':
            nonmandatory_tags['CMP_PeerVPC'] = parameter['ParameterValue']
        elif parameter['ParameterKey'] == 'VPCOptions':
            nonmandatory_tags['CMP_VPCOptions'] = parameter['ParameterValue']

    #Fetch organization id:
    organization_response = organization_client.describe_organization()
    organization_id = organization_response['Organization']['Id']


    if accountid_value not in accounts_list:
        print(f"stack account id {accountid_value} is not in the accounts list")
        return
    
    #check the account is present or not
    paginator = organization_client.get_paginator("list_tags_for_resource")
    pages = paginator.paginate(ResourceId=accountid_value)

    #Get the keys of tags in account
    key_list = []
    for page in pages:
        if len(page["Tags"]) != 0:
            for key in page['Tags']:
                key_list.append(key['Key'])
     
    print(f"Keys present in account {accountid_value} {key_list}")
    
    missing_mandatory_tags = []
    #updating Mandatory tags
    for tag in mandatory_tags_list:
        if len(key_list) == 0:
            try:
                print(f"Tagging account {accountid_value} with mandatory tag {tag}")
                response = organization_client.tag_resource(
                    ResourceId=accountid_value,
                    Tags=[
                        {
                        'Key': tag,
                        'Value': mandatory_tags[tag] 
                    },]
                )
            except KeyError as e :
                print(e)
                missing_mandatory_tags.append(tag)
        else:
            if tag not in key_list:
                try:
                    print(f"Tagging account {accountid_value} with mandatory tag {tag}")
                    response = organization_client.tag_resource(
                        ResourceId=accountid_value,
                        Tags=[
                            {
                            'Key': tag,
                            'Value': mandatory_tags[tag] 
                        },]
                    )
                except KeyError as e :
                    print(e)
                    missing_mandatory_tags.append(tag)
                
    
    print(f"missing_mandatory_tags for {accountid_value}",missing_mandatory_tags)
    if len(missing_mandatory_tags) != 0:
        cf_account_list.append(accountid_value)
        row = [accountid_value , stack_name]
        tags_string = "#".join(missing_mandatory_tags)
        row.append(tags_string)
        new_line = ",".join(row) + "\n"
        if new_line[-1] != "\n":
            new_line = new_line + "\n"
        tags_account_msg = tags_account_msg + new_line

                                
    #updating Non Mandatory tags  
    for tag in nonmandatory_tags:
        if len(key_list) == 0:
            update_non_mandatory_tags(tag,nonmandatory_tags,accountid_value)
        else:
            if tag not in key_list:
                update_non_mandatory_tags(tag,nonmandatory_tags,accountid_value)
    
    return tags_account_msg,cf_account_list  

#Get OU Id and Name stack info  
def get_parent(UID):
    try:
        print(UID)
        list_parents = organization_client.list_parents(ChildId=UID)
        for parent in list_parents['Parents']:
            ou_Id = parent['Id']
            ou_type = parent['Type']
            ou_name = get_unit_name(ou_Id)
        return ({'ou_Id': ou_Id, 'ou_name': ou_name})
    except:
        print (f"root account would not have parents or account {accountid_value} is not present") 
        return ({'ou_Id': None, 'ou_name': None})

def get_unit_name(ouId):
    try:
       desc_ou = organization_client.describe_organizational_unit(OrganizationalUnitId=ouId) 
       ou_name = desc_ou['OrganizationalUnit']['Name']
       return (ou_name)
    except:
       return None
       
def publish_message(msg,sub):
    response = sns_client.publish(
        TopicArn=sns_topic_arn,
        Message=msg,
        Subject=sub
    )

def update_non_mandatory_tags(tag,nonmandatory_tags,accountid_value):
    try:
        print(f"Tagging account {accountid_value} with non-mandatory tags {nonmandatory_tags[tag]}")
        response = organization_client.tag_resource(
            ResourceId=accountid_value,
                Tags=[
                {
                    'Key': tag,
                    'Value': nonmandatory_tags[tag] 
                },
            ]
        )
    except Exception as e :
        print(e)

