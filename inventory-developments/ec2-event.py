import os
import json
import sys 
import boto3
import datetime
import time
import botocore
from   botocore.exceptions import ClientError
from   boto3.dynamodb.conditions import Key 
# Get the service resource.
TABLE_NAME = os.environ['TABLE_NAME']
dynamodb_resource = boto3.resource('dynamodb')
dynamodb_client = boto3.client('dynamodb')
table = dynamodb_resource.Table(TABLE_NAME)
mandatorytags = os.environ['MANDATORY_TAGS']
mandatory_tags_list = mandatorytags.split(',')
cloudops_dl = os.environ['CLOUDOPS_DL']
cloudops_tag = cloudops_dl.split('=')



def lambda_handler(event, context):
    print(event)
    if (event['source'] == "aws.ec2" and event['detail-type'] == "EC2 Instance State-change Notification"):
        instance_state = instance_state_update(event)
    elif (event['source'] == "aws.ec2" and event['detail-type'] == "AWS API Call via CloudTrail"):
        eventnamekey = event['detail']['eventName']
        if eventnamekey == "ModifyInstanceAttribute" :
            instance_type = instance_type_update(event) 
            print(instance_type)
        elif eventnamekey == "CreateTags" :
            resourceidkey = event['detail']['requestParameters']['resourcesSet']['items'][0]['resourceId']
            resourceid = resourceidkey
            if  resourceid.startswith("i-"):
                modify_instance_tags = instance_tag_update(event)
                print(modify_instance_tags)
        elif eventnamekey == "DeleteTags" :
            resourceidkey = event['detail']['requestParameters']['resourcesSet']['items'][0]['resourceId']
            resourceid = resourceidkey
            if  resourceid.startswith("i-"):
                delete_instance_tags = instance_tag_delete(event)
                print(delete_instance_tags)
        elif eventnamekey == "RunInstances" :
            new_instance_creation = instance_create(event)
            print(new_instance_creation)
        elif eventnamekey == "AttachNetworkInterface" :
            new_network_interface_creation = network_interface_create(event)
            print(new_network_interface_creation)
        elif eventnamekey == "AttachVolume":
            volume_change = volume_event(event)   
            print(volume_change)
        elif eventnamekey == "DetachVolume":
            volume_change = volume_event(event)   
            print(volume_change)
        else :
            print("New event names will be added")
    else:
        print("Not handling this event")



def instance_state_update(event):
    current_time = get_time()
    try:
        update_item = table.update_item(
        Key={
            'ResourceId': event['detail']['instance-id'],
            'region': event['region']
        },
        UpdateExpression='SET current_status = :value1, lastmodifieddate = :value2',
        ExpressionAttributeValues={
            ':value1': event['detail']['state'],
            ':value2': current_time
        }
        )
    except Exception as e:
        print(e)
        update_item = e
        return update_item
    if event['detail']['state'] == 'terminated':
        time.sleep(5)
        try:
            delete_item = table.delete_item(
            Key={
                'ResourceId': event['detail']['instance-id'],
                'region': event['region']
            } )
        except Exception as e:
            print(e)
    return update_item
    
def instance_type_update(event):
    instanceidkey = event['detail']['requestParameters']['instanceId']
    current_time = get_time()
    try:
        update_item = table.update_item(
        Key={
            'ResourceId': instanceidkey,
            'region': event['region']
        },
        UpdateExpression='SET instanceType = :value1, lastmodifieddate = :value2',
        ExpressionAttributeValues={
            ':value1': event['detail']['requestParameters']['instanceType']['value'],
            ':value2': current_time
        }
        )
    except Exception as e:
        print(e)
        update_item = e
        return update_item
    except KeyError:
        print("Unknown modify parameter")
    return update_item
    
def instance_tag_update(event):
    resourceidkey = event['detail']['requestParameters']['resourcesSet']['items'][0]['resourceId']
    try:
        response =  table.query(
            KeyConditionExpression=Key('ResourceId').eq(resourceidkey)
        )
        item = response['Items'][0]
        mandatory_tags = response['Items'][0]['mandatory_tags']
        non_mandatory_tags = response['Items'][0]['non_mandatory_tags']
        input_tags = event['detail']['requestParameters']['tagSet']['items']
        non_man_tags_list = []
        for tag in input_tags:
            if tag["key"] in mandatory_tags_list:
                for items in item['mandatory_tags']:
                    if items['tag_name'] == tag["key"]:
                        item['mandatory_tags'].remove({ "tag_name" : items['tag_name']  , "tag_value" : items['tag_value'] })
                        item['mandatory_tags'].append({ "tag_name" : tag["key"]  , "tag_value" : tag["value"] })
                        item[tag["key"]] = tag["value"]
            else :
                non_man_tag_key_list = []
                for items in item['non_mandatory_tags']:
                    non_man_tag_key_list.append(items['tag_name'])
                if tag["key"] in non_man_tag_key_list:
                    for items in item['non_mandatory_tags']:
                        if items['tag_name'] == tag["key"]:
                            item['non_mandatory_tags'].remove({ "tag_name" : items['tag_name']  , "tag_value" : items['tag_value'] })
                            item['non_mandatory_tags'].append({ "tag_name" : tag["key"]  , "tag_value" : tag["value"] })
                else:
                    item['non_mandatory_tags'].append({ "tag_name" : tag["key"]  , "tag_value" : tag["value"] })
    except Exception as e:
        print(e)
        response = e
        return response
    if  response['Count'] == 0 :
        return response
    current_time = get_time()
    item['lastmodifieddate'] = current_time

    try:
        put_item = table.put_item(
            Item=item
        )
    except Exception as e:
        print(e)
        response = e
        return response
    return put_item
            
def instance_tag_delete(event):
    resourceidkey = event['detail']['requestParameters']['resourcesSet']['items'][0]['resourceId']
    try:
        response =  table.query(
            KeyConditionExpression=Key('ResourceId').eq(resourceidkey)
        )
        item = response['Items'][0]
        mandatory_tags = response['Items'][0]['mandatory_tags']
        non_mandatory_tags = response['Items'][0]['non_mandatory_tags']
        input_tags = event['detail']['requestParameters']['tagSet']['items']
        for tag in input_tags:
            if tag["key"] in mandatory_tags_list:
                for items in item['mandatory_tags']:
                    if items['tag_name'] == tag["key"]:
                        item['mandatory_tags'].remove({ "tag_name" : items['tag_name']  , "tag_value" : items['tag_value'] })
                        item['mandatory_tags'].append({ "tag_name" : tag["key"]  , "tag_value" : "" })
                        item[tag["key"]] = ""
            else :
                for items in item['non_mandatory_tags']:
                    if items['tag_name'] == tag["key"]:
                        item['non_mandatory_tags'].remove({ "tag_name" : items['tag_name']  , "tag_value" : items['tag_value'] })
    except Exception as e:
        print(e)
        response = e
        return response
    if  response['Count'] == 0:
        return response
    current_time = get_time()
    item['lastmodifieddate'] = current_time
    
    try:
        put_item = table.put_item(
            Item=item
        )
    except Exception as e:
        print(e)
        put_item = e
        return put_item
    return put_item

    
    
def instance_create(event): 
    relationship_list = []
    accountId = event['account']
    region = event['region']
    try:
        resourceset = event['detail']['responseElements']['instancesSet']['items']
    except TypeError as e:
        exception = e
        return exception        
    ResourceId = resourceset[0]['instanceId']
    resourceType = 'AWS::EC2::Instance'
    resourceCreationTime = event['time']
    current_status = resourceset[0]['instanceState']['name']
    ImageId = resourceset[0]['imageId']
    arn = 'arn:aws:ec2:us-east-1:' + str(accountId) + ':instance/' + str(ResourceId)
    availabilityZone = resourceset[0]['placement']['availabilityZone']
    instanceType = resourceset[0]['instanceType']
    privateIpAddress = resourceset[0]['privateIpAddress']
    privateDnsName = resourceset[0]['privateDnsName']
    vpcId = resourceset[0]['vpcId']
    relationship_list = append_relationship(vpcId,relationship_list)
    subnetId = resourceset[0]['subnetId']
    relationship_list = append_relationship(subnetId,relationship_list)
    NetworkInterface_list = []
    for network_id in resourceset[0]['networkInterfaceSet']['items']:
        networkid = {}
        networkid["S"] = network_id['networkInterfaceId'] 
        NetworkInterface_list.append(networkid)
        relationship_list = append_relationship(network_id['networkInterfaceId'],relationship_list)
    NetworkInterface = NetworkInterface_list
    volume_list = []
    try:
        volume_list_present = event['detail']['requestParameters']['blockDeviceMapping']['items']
        rootdevice_name = resourceset[0]['rootDeviceName']
        for vol in volume_list_present:
            if vol['deviceName'] == rootdevice_name:
                rootdevicestatus = "True"
            else:
                rootdevicestatus = "False"
            volume_structure = {
            "deviceName": {'S': vol['deviceName']},
            "rootdevicestatus": {'S': rootdevicestatus},
            "volumeType": {'S': vol['ebs']['volumeType']},
            "volumesize": {'S': str(vol['ebs']['volumeSize'])}
            }
            volume_struct = { "M" : volume_structure }
            volume_list.append(volume_struct)
    except Exception as e:
        print(e)
    sg_list = []
    for sg in resourceset[0]['groupSet']['items']:
        sg_id = {}
        sg_id["S"] = sg['groupId']
        sg_list.append(sg_id)
        relationship_list = append_relationship(sg['groupId'],relationship_list)
    try:
        tag_set = resourceset[0]['tagSet']['items']
        mandatory_tags = fetch_mandatory_tags(tag_set)
        non_mandatory_tags = fetch_non_mandatory_tags(tag_set)
    except KeyError as e:
        non_mandatory_tags = []
        mandatory_tags= []
        for tag in mandatory_tags_list:
            tag_dict = { "tag_name" : { "S" : tag } , "tag_value" : { "S" : "" } }
            if tag == cloudops_tag[0]:
                tag_dict = { "tag_name" : { "S" : tag } , "tag_value" : { "S" : cloudops_tag[1] } }
            tag_struct_dict = { "M" : tag_dict }
            mandatory_tags.append(tag_struct_dict)
    current_time = get_time()
    item={
                "accountid"                         : { "S": accountId           }, 
                "ResourceId"                        : { "S": ResourceId          }, 
                "region"                            : { "S": region          }, 
                "resourceType"                      : { "S": resourceType        }, 
                "resourceCreationTime"              : { "S": str(resourceCreationTime) } ,
                "lastmodifieddate"                  : { "S": current_time},
                "current_status"                    : { "S": current_status },
                "ImageId"                           : { "S": ImageId},
                "arn"                               : { "S": arn     }, 
                "availabilityZone"                  : { "S": availabilityZone }, 
                "instanceType"                      : { "S": instanceType     },
                "privateIpAddress"                  : { "S": privateIpAddress     },
                "privateDnsName"                    : { "S": privateDnsName     },
                "vpcId"                             : { "S": vpcId     },
                "subnetId"                          : { "S": subnetId },
                "NetworkInterface"                  : { "L"  : NetworkInterface     },
                "Volumes"                           : { "L"   : volume_list },
                "relationships"                     : { "L": relationship_list },
                "securitygroups"                    : { "L": sg_list},
                "mandatory_tags"                    : { "L":  mandatory_tags},
                "non_mandatory_tags"                : { "L":  non_mandatory_tags},
                "created_by_event"                  : { "S": "true" }
        }
    for tag in mandatory_tags:
        man_tag = { tag["M"]["tag_name"]["S"] : tag["M"]["tag_value"] }
        item.update(man_tag)
    try:
      Update_Table = dynamodb_client.put_item(
           TableName=TABLE_NAME,
           Item=item
       )
    except Exception as e:
        print(e)
        Update_Table = e
        return Update_Table
    return Update_Table
    
        

def fetch_mandatory_tags(tag_set):
    man_tags = []
    tags_present_list = []
    for tag_present in tag_set:
        tags_present_list.append(tag_present['key'])
    tag_key_value = {}
    for tag in tag_set:
        tag_key_value.update({ tag['key'] : tag['value']})
    for tag in mandatory_tags_list:
        if tag in tags_present_list:
            tag_dict = { "tag_name" : { "S" : tag } , "tag_value" : { "S" : tag_key_value[tag] } }
            tag_struct_dict = { "M" : tag_dict }
            man_tags.append(tag_struct_dict)
        else:
            if tag == cloudops_tag[0]:
                tag_dict = { "tag_name" : { "S" : tag } , "tag_value" : { "S" : cloudops_tag[1] } }
            else:
                tag_dict = { "tag_name" : { "S" : tag } , "tag_value" : { "S" : "" } }
            tag_struct_dict = { "M" : tag_dict }
            man_tags.append(tag_struct_dict)
    return man_tags
    
def fetch_non_mandatory_tags(tag_set):
    non_man_tags = []
    for tag in tag_set:
        if ( tag['key'] not in mandatory_tags_list ) :
            tag_dict = { "tag_name" : { "S" : tag['key'] } , "tag_value" : { "S" : tag['value'] } }
            tag_struct_dict = { "M" : tag_dict }
            non_man_tags.append(tag_struct_dict)
    return non_man_tags
    
def append_relationship(resource_id,relationship_list):
    if resource_id[0:4] == 'vpc-':
        relationship = {
                "relationshipName": { "S" : "Is contained in Vpc" }, 
                "resourceId": { "S" : resource_id } , 
                "resourceType": { "S" : "AWS::EC2::VPC" }
            }
        relationship_dict = { "M" : relationship }
        relationship_list.append(relationship_dict)
    elif resource_id[0:4] == 'eni-':
        relationship = {
                "relationshipName": { "S" : "Contains NetworkInterface" },
                "resourceId": { "S" : resource_id } , 
                "resourceType": { "S" : "AWS::EC2::NetworkInterface" }
            }
        relationship_dict = { "M" : relationship }
        relationship_list.append(relationship_dict)
    elif resource_id[0:3] == 'sg-':
        relationship = {
                "relationshipName": { "S" : "Is associated with SecurityGroup" }, 
                "resourceId": { "S" : resource_id }, 
                "resourceType": { "S" : "AWS::EC2::SecurityGroup" }
            }
        relationship_dict = { "M" : relationship }
        relationship_list.append(relationship_dict)
    elif resource_id[0:7] == 'subnet-': 
        relationship = {
                "relationshipName": { "S" : "Is contained in Subnet" }, 
                "resourceId": { "S" : resource_id }, 
                "resourceType": { "S" : "AWS::EC2::Subnet" }
            }
        relationship_dict = { "M" : relationship }
        relationship_list.append(relationship_dict)
    else:
        print("New relationship")
        
    return relationship_list
    
def network_interface_create(event):
    instanceidkey = event['detail']['requestParameters']['instanceId']
    try:
        response =  table.query(
            KeyConditionExpression=Key('ResourceId').eq(instanceidkey)
        )
        print(response)
        NetworkInterface = list(response['Items'][0]['NetworkInterface'])
        networkid = event['detail']['requestParameters']['networkInterfaceId']
        NetworkInterface.append(networkid)
    except Exception as e:
        print(e)
        response = e
        return response
    if  response['Count'] == 0:
        return response
    current_time = get_time()
    try:
        update_item = table.update_item(
        Key={
            'ResourceId': instanceidkey,
            'region': event['region']
        },
        UpdateExpression='SET NetworkInterface = :value1, lastmodifieddate = :value2',
        ExpressionAttributeValues={
            ':value1': NetworkInterface,
            ':value2': current_time
        }
        )
    except Exception as e:
        print(e)
        update_item = e
    return update_item
    
def volume_event(event):
    eventnamekey = event['detail']['eventName']
    instanceidkey = event['detail']['requestParameters']['instanceId']
    try:
        response =  table.query(
            KeyConditionExpression=Key('ResourceId').eq(instanceidkey)
        )
        volume_list = response['Items'][0]['Volumes']
        if eventnamekey == 'AttachVolume':
            volume_dict = {
                "volumeId" : event['detail']['responseElements']['volumeId'],
                "deviceName" : event['detail']['responseElements']['device']
            }
            volume_list.append(volume_dict)
        else:
            for vol in volume_list:
                if (event['detail']['responseElements']['volumeId'] == vol['volumeId']):
                    delete_vol = vol
            volume_list.remove(delete_vol)
    except Exception as e:
        print(e)
        response = e
        return response
    if  response['Count'] == 0 :
        return response
    current_time = get_time()
    try:
        update_item = table.update_item(
        Key={
            'ResourceId': instanceidkey,
            'region': event['region']
        },
        UpdateExpression='SET Volumes = :value1, lastmodifieddate = :value2',
        ExpressionAttributeValues={
            ':value1': volume_list,
            ':value2': current_time
        }
        )
    except Exception as e:
        print(e)
        update_item = e
        return update_item
    return update_item
    
def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    formatedtime = "T" + str(Time) + "Z"
    Now = ("%s%s" %(Today,formatedtime))
    return(Now) 

