import json
import boto3
import botocore
import botocore.exceptions
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
from awsretry import AWSRetry
import os
import sys
import datetime
import time 
import Volume
import DynamoDB

table_name = os.environ['TABLE_NAME']
dynamodbr = boto3.resource('dynamodb')
table = dynamodbr.Table(table_name)
aggregator_name = os.environ['AGGREGATOR_NAME']
mandatorytags = os.environ['MANDATORY_TAGS']
region_list = os.environ['AGGREGATOR_REGIONS'] 
mandatory_tags = mandatorytags.split(',')
stream_table_name = os.environ['STREAM_TABLE_NAME']
stream_dynamodbr = boto3.resource('dynamodb')
stream_table = stream_dynamodbr.Table(stream_table_name)
primary_key = 'ResourceId'
tag_name = "tag_name"
tag_value = "tag_value"
client  = boto3.client('config')

def lambda_handler(event, context):
    #Fetch the count of db records 
    reg_record = {}
    resource_id_list = []
    db_resource_count = 0
    
    for reg in region_list.split(","):
        reg_record[reg] = QueryIndex("region-Index","region",reg)
        if reg_record[reg]['Count'] == 0:
            db_resource_count = 0  
        else:
            record_list = reg_record[reg]['Items']
            for resid in record_list:
                resource_id_list.append(resid[primary_key])
                db_resource_count += 1

    resource_type = 'AWS::EC2::Instance'
    list_ec2 = aggregate_discovered_resources(resource_type)
    ec2_resource = list_ec2['ResourceIdentifiers']
    item_resource_count = 0
    item_resource_id = []
    while True:
        for ec2_info in ec2_resource:
            fetch_ec2 = fetch_ec2_info(ec2_info,reg_record)
            item_resource_count += 1
            item_resource_id.append(ec2_info[primary_key])
            if fetch_ec2["ResponseMetadata"]["HTTPStatusCode"] == 200:
                print ("Successfully fetched ec2_info " + ec2_info[primary_key])
            else:
                print ("failed to fetch ec2_info" + ec2_info[primary_key])

        if 'NextToken' in list_ec2:
            list_ec2 = aggregate_discovered_resources_next_token(resource_type,list_ec2['NextToken'])
            ec2_resource = list_ec2['ResourceIdentifiers']
        else:
            print("Aggregator resource count", item_resource_count)
            print("Database resource count", db_resource_count)
            break

    #if db is empty
    if db_resource_count == 0:
        return
    
    # Delete additional records in table which are not in config aggregator
    
    reg_dict = {}
    for reg in region_list.split(","):
        resource_id_list_l = []
        record_list = reg_record[reg]['Items']
        for resid in record_list:
            resource_id_list_l.append(resid[primary_key]) 
        reg_dict[reg] = resource_id_list_l
            
    for delitem in resource_id_list:
        if delitem not in item_resource_id:
            print("record to be deleted: ",delitem)
            try:
                response =  table.query(
                    KeyConditionExpression=Key(primary_key).eq(delitem)
                )
                created_by_event = response['Items'][0]['created_by_event']
                stream_response =  stream_table.query(
                    KeyConditionExpression=Key(primary_key).eq(delitem)
                )
                stream_response_list = stream_response['Items']
                status_list = []
                for item in stream_response_list:
                    status_list.append(item['current_status'])
                if 'terminated' in status_list:
                    item_terminated = True
                else:
                    item_terminated = False
                if ((created_by_event == 'true') and not(item_terminated)):
                    print("created by event, so skipping to delete the record")
                else:
                    for reg in region_list.split(","):
                        if delitem in reg_dict[reg]:
                            del_region = reg
                            deleteitem = delete_item(delitem,del_region)
                            print(deleteitem)
            except Exception as e:
                print(e)
            
            

         

@AWSRetry.backoff(tries=20, delay=2, backoff=1.5, added_exceptions=['Rate exceeded'])   	        
def fetch_ec2_info(ec2_info,reg_record):
    try:
        ec2_response = client.get_aggregate_resource_config(
            ConfigurationAggregatorName=aggregator_name,
            ResourceIdentifier={
                'SourceAccountId': ec2_info['SourceAccountId'],
                'SourceRegion': ec2_info['SourceRegion'],
                'ResourceId': ec2_info[primary_key],
                'ResourceType': 'AWS::EC2::Instance'
                }
        )
    except ClientError as error:
        print(error)
        raise error


    ec2_resource_information = ec2_response['ConfigurationItem']
    print(ec2_resource_information)
    volume_data_dict,volume_data_dict_db = Volume.fetch_volume_info(ec2_resource_information)
    update_db = DynamoDB.UpdateDB(ec2_resource_information,volume_data_dict,volume_data_dict_db,reg_record)
    return(ec2_response)
    
@AWSRetry.backoff(tries=20, delay=2, backoff=1.5, added_exceptions=['Rate exceeded'])
def aggregate_discovered_resources(resource_type):
    try:
        response = client.list_aggregate_discovered_resources(
            ConfigurationAggregatorName=aggregator_name,
            ResourceType=resource_type
        )
    except ClientError as error:
        print(error)
        raise error

    return(response)

@AWSRetry.backoff(tries=20, delay=2, backoff=1.5, added_exceptions=['Rate exceeded'])
def aggregate_discovered_resources_next_token(resource_type,nexttoken):
    try:
        response = client.list_aggregate_discovered_resources(
            ConfigurationAggregatorName=aggregator_name,
            ResourceType=resource_type,
            NextToken=nexttoken
        )
    except ClientError as error:
        print(error)
        raise error

    return(response)
  
 
def fetch_ni_list(ec2_detailed_information_load):
    networkInterface_list = []
    networkInterface_db = []
    sg_list = []
    sg_db = []
    for network_id in ec2_detailed_information_load["networkInterfaces"]:
        networkid = {}
        networkid["S"] = network_id["networkInterfaceId"]
        networkInterface_list.append(networkid)
        networkInterface_db.append(network_id["networkInterfaceId"])
    for sg in ec2_detailed_information_load["securityGroups"]:
        sgid = {}
        sgid["S"] = sg["groupId"]
        sg_list.append(sgid) 
        sg_db.append(sg["groupId"])
    return networkInterface_list,sg_list,networkInterface_db,sg_db
    
def fetch_relationships(ec2_detailed_information): 
    relationship_list = []
    for relationship in ec2_detailed_information["relationships"]:
        relationshipName = relationship["relationshipName"]
        resourceId       = relationship["resourceId"]
        resourceType     = relationship["resourceType"]
        relationship_dict = {
                "relationshipName": { "S" : relationshipName }, 
                "resourceId": {"S" : resourceId }, 
                "resourceType": { "S" : resourceType }
            }
        relationship_dict_list = { "M" : relationship_dict }
        relationship_list.append(relationship_dict_list)
    return relationship_list
    
def fetch_mandatory_tags(ec2_detailed_information):
    tags_present = ec2_detailed_information["tags"]
    tags_present_list = list(ec2_detailed_information["tags"].keys()) 
    print("tags_present_list", tags_present_list)
    man_tags = []
    man_tags_db = []
    for tag in mandatory_tags:
        if tag in tags_present_list:
            tag_dict = { tag_name : { "S" : tag } , tag_value : { "S" : tags_present[tag] } }
            tag_dict_db = { tag_name : tag  , tag_value : tags_present[tag] }
            tag_struct_dict = { "M" : tag_dict }
            man_tags.append(tag_struct_dict)
            man_tags_db.append(tag_dict_db)
        else:
            if tag == 'cloudops':
                tag_dict = { tag_name : { "S" : tag } , tag_value : { "S" : "CTI GL CIS PUBLIC CLOUD" } }
                tag_dict_db = { tag_name : tag  , tag_value : "CTI GL CIS PUBLIC CLOUD" }
            else:
                tag_dict = { tag_name : { "S" : tag } , tag_value : { "S" : "" } }
                tag_dict_db = { tag_name : tag  , tag_value : "" }
            tag_struct_dict = { "M" : tag_dict }
            man_tags.append(tag_struct_dict)
            man_tags_db.append(tag_dict_db)
    return man_tags,man_tags_db
    
def fetch_non_mandatory_tags(ec2_detailed_information):
    tags_present = ec2_detailed_information["tags"]
    tags_present_list = list(ec2_detailed_information["tags"].keys()) 
    non_man_tags = []
    non_man_tags_db = []
    for tag in tags_present_list:
        if tag not in mandatory_tags:
            tag_dict = { tag_name : { "S" : tag } , tag_value : { "S" : tags_present[tag] } }
            tag_dict_db = { tag_name : tag  , tag_value : tags_present[tag] }
            tag_struct_dict = { "M" : tag_dict }
            non_man_tags.append(tag_struct_dict)
            non_man_tags_db.append(tag_dict_db)
    return non_man_tags,non_man_tags_db
    
    
def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    formatedtime = "T" + str(Time) + "Z"
    Now = ("%s%s" %(Today,formatedtime))
    return(Now) 
            
    
        
def QueryIndex(index_name,index,pkey):

    try:
         response = table.query(IndexName=index_name, KeyConditionExpression=Key(index).eq(pkey))
    except Exception as e:
         print('Error performing Query')
         response = e
    return response
    
def delete_item(delitem,reg):
    print("Deleting record for " , delitem )
    try:
        response = table.delete_item(
            Key={
                primary_key : delitem,
                'region': reg
            }
        )
    except Exception as e:
         print('Error performing Delete item')
         response = e
    return response
    
        
