import boto3
import os
import datetime
import time
from   boto3.dynamodb.conditions import Key 

dynamodb = boto3.client('dynamodb')
dynamodb_resource = boto3.resource('dynamodb')
dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]
table = dynamodb_resource.Table(dynamodb_table)

ayearafter = datetime.datetime.today() + datetime.timedelta(days=365)
expiryDateTime = int(time.mktime(ayearafter.timetuple()))


def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    formatedtime = "T" + str(Time) + "Z"
    Now = ("%s%s" %(Today,formatedtime))
    return(Now) 

def lambda_handler(event, context):
    print('------------------------')
    print(event)
    try:
        for record in event['Records']:
            if record['eventName'] == 'INSERT':
                handle_insert(record)
            elif record['eventName'] == 'MODIFY':
                handle_modify(record)
            elif record['eventName'] == 'REMOVE':
                handle_remove(record)
        print('------------------------')
        return "Success!"
    except Exception as e: 
        print(e)
        print('------------------------')
        return "Error"


def handle_insert(record):
    print("Handling INSERT Event")
    #Insert = {}
    newImage = record['dynamodb']['NewImage']
    print(newImage)
    print ('New Instance= ' + newImage['ResourceId']['S'] + ' with status= ' + newImage['current_status']['S'])
    current_time = get_time()
    time_dict = { "lastmodifieddate" : { "S" : current_time } }
    newImage.update(time_dict)
    response = update_stream_table(newImage)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print("DB Update was successful")
    else:
        print("DB Update failed")


def handle_modify(record):
    print("Handling MODIFY Event")
    newImage = record['dynamodb']['NewImage']
    print(newImage)
    print ('Modifying Instance= ' + newImage['ResourceId']['S'] + ' with status= ' + newImage['current_status']['S'])
    current_time = get_time()
    time_dict = { "lastmodifieddate" : { "S" : current_time } }
    if newImage['current_status']['S'] == 'terminated':
        return
    newImage.update(time_dict)
    response = update_stream_table(newImage)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print("DB Update was successful")
    else:
        print("DB Update failed")


def handle_remove(record):
    print("Handling REMOVE Event")
    oldImage = record['dynamodb']['OldImage']
    print ('Deleting Instance= ' + oldImage['ResourceId']['S'] + ' with status= ' + oldImage['current_status']['S'])
    current_time = get_time()
    time_dict = { "lastmodifieddate" : { "S" : current_time } }
    timetoexist = { "timetoexist" : { 'N': str(expiryDateTime) } }
    oldImage.update(time_dict)
    oldImage.update(timetoexist)
    try:
        ttl_records =  table.query(
            KeyConditionExpression=Key('ResourceId').eq(oldImage['ResourceId']['S'])
        )
    except Exception as e:
        print(e)
    print(ttl_records)
    for record in ttl_records['Items']:
        update_ttl = update_ttl_records(record)
        print(update_ttl)
    response = update_stream_table(oldImage)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print("DB Update was successful")
    else:
        print("DB Update failed")

    
def update_stream_table(data):
    try:
        Update_Table = dynamodb.put_item(
            TableName=dynamodb_table,
            Item=data
        )
    except Exception as e: 
        Update_Table = e
        print(Update_Table)
        return (Update_Table)
    return (Update_Table)


def update_ttl_records(record):
    try:
        update_item = dynamodb.update_item(
            Key={
                'ResourceId': {
                    'S': record['ResourceId'],
                },
                'lastmodifieddate': {
                    'S': record['lastmodifieddate'],
                },
            },
            ExpressionAttributeNames={
                '#Y': 'timetoexist',
            },
            UpdateExpression='SET #Y = :y',
            ExpressionAttributeValues={
                ':y': {
                    'N': str(expiryDateTime),
                },
            },
            TableName=dynamodb_table
        )
    except Exception as e:
        print(e)
        update_item = e
        return update_item
    return update_item
    
    
