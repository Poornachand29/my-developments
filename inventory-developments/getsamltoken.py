#!/usr/bin/env python3

# stock dependencies
# found in python38.zip on Windows
import os
import re
import sys
import json
import base64
import getpass
import argparse
import configparser
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

# additional dependencies
# bundled in dependencies.zip, except for botocore
# https://github.com/boto/boto3/issues/1770
runtime = os.path.dirname(os.path.abspath(__file__))
dep = os.path.join(runtime, "dep")
sys.path.append(os.path.join(dep, "dependencies.zip"))
sys.path.append(dep)

import boto3
#boto3.set_stream_logger(name='botocore')
import requests
from dateutil.tz import *
from bs4 import BeautifulSoup

# TODO: identify platform Windows vs Linux?

# Credits:
# https://aws.amazon.com/blogs/security/how-to-implement-federated-api-and-cli-access-using-saml-2-0-and-ad-fs/
# Ming Li
# Zack McClary
# David Gubitosi


def configure_argparse():
    # Command line arguments
    parser = argparse.ArgumentParser()

    # sso env choice, defaults to prod
    sso = parser.add_mutually_exclusive_group()
    sso.add_argument('--sso-dev',
                    help='Authenticate against DEV SSO',
                    action='store_const',
                    default=2,
                    const=0,
                    dest='selectedEnv')
    sso.add_argument('--sso-uat',
                    help='Authenticate against UAT SSO',
                    action='store_const',
                    const=1,
                    dest='selectedEnv')
    sso.add_argument('--sso-prod',
                    help='Authenticate against PROD SSO (default)',
                    action='store_const',
                    const=2,
                    dest='selectedEnv')

    # force the sso env menu
    # supercedes --sso-{env} selection
    # enable Dev, disabled by default
    sso.add_argument('--sso-menu',
                        help=argparse.SUPPRESS,
                        action='store_true',
                        default=False,
                        dest='ssoMenu')

    # SSO username
    parser.add_argument('--user',
                        help='SSO username (optional, defaults to current user)',
                        nargs='?',
                        default=os.getlogin(),
                        action='store',
                        dest='username')

    # AWS CLI profile support
    parser.add_argument('--profile',
                        help='AWS CLI profile (optional, defaults to default profile)',
                        default='default',
                        action='store',
                        dest='profile')

    # Role ARN
    parser.add_argument('--role-arn',
                        help='Role ARN to assume (optional, menu will be displayed if omitted)',
                        action='store',
                        default=None,
                        dest='role_arn')

    # Alternate proxy for API access
    parser.add_argument('--proxy',
                        help='Set proxy, default is cspnaproxy1.wlb2.nam.nsroot.net:8888',
                        action='store',
                        dest='proxy')

    # Dump SAML assertion
    parser.add_argument('--saml-debug',
                        help=argparse.SUPPRESS,
                        action='store_true',
                        default=False,
                        dest='samlDebug')

    subparsers = parser.add_subparsers(dest='subparser_name')
    parser_auth = subparsers.add_parser('auth')

    # AWS CLI credential helper support
    parser_creds = subparsers.add_parser('credential-helper',
        help='Enables AWS CLI credential helper mode'
    )

    parser_list_roles = subparsers.add_parser('list-roles',
        help='List AWS roles available to the user'
    )

    return parser


def list_roles(opts, awsroles):
    awsroles.sort()
    for i, awsrole in enumerate(awsroles):
        arn = awsrole.split(',')[0]
        print(arn)


parser = configure_argparse()
opts = parser.parse_args()

username = None
if opts.username is not None:
    username = opts.username

proxy = 'cspnaproxy1.wlb2.nam.nsroot.net:8888'
if opts.proxy is not None:
    proxy = opts.proxy

# python os.environ does not alter the shell so we can override them here
for p in [ 'http_proxy', 'https_proxy' ]:
    os.environ[p] = proxy
    os.environ[p.upper()] = proxy

# use NO_PROXY to support the IdP connection
no_proxy = '.nsroot.net,.citigroup.net,.citi.com,.ssmb.com'
os.environ['no_proxy'] = no_proxy
os.environ['NO_PROXY'] = no_proxy

# force menu
if opts.ssoMenu:
    enabledEnvs = [ 0, 1, 2 ]
# otherwise use the --sso-{env} selection
else:
    enabledEnvs = [ opts.selectedEnv ]

# region: The default AWS region that this script will connect for all API calls
region = 'us-east-1'

# output format: The AWS CLI output format that will be configured in the
# saml profile (affects subsequent CLI calls)
outputformat = 'json'

# aws credentials file location
home = os.path.expanduser("~")
awsDir = os.path.join(home, '.aws')
credFile = os.path.join(awsDir, 'credentials')

#sslverificattion: CA file
v = requests.__version__.split('.')
if int(v[1]) >= 16:
    sslverification = True
else:
    sslverification = os.path.join(dep, "sso-cacerts.pem")

# force all users back to embedded cert for now
sslverification = os.path.join(dep, "sso-cacerts.pem")

# seperator
sep = '-'*70

# Used to build the SSO environment menu
ENVS = [ 'DEV', 'UAT', 'PROD' ]
idpHOSTS = [
    'https://secureaccess.sit.nam.citigroup.net',
    'https://secureaccess.uat.nam.citigroup.net',
    'https://secureaccess.nam.citigroup.net'
]
idpRoot = '/idp/startSSO.ping?PartnerSpId=urn:amazon:webservices'

# build IDPS dict based on the enabledEnvs
IDPS = {}
for i in enabledEnvs:
    IDPS[ENVS[i]] = idpHOSTS[i]

parentShell = 'None'
if os.getenv('OS', '') == 'Windows_NT':
    # when powershell is active, there are at least three module paths
    if len(os.getenv('PSModulePath', '').split(os.pathsep)) >= 3:
        parentShell = 'PS'
    else:
        parentShell = 'CMD'
else:
    # Linux so assume a posix shell
    parentShell = 'POSIX'

OUTPUT = {
        'PS': [ 'bat', '@{}', 'set {}={}' ],
        'CMD': [ 'bat', '@{}', 'set {}={}' ],
        'POSIX': [ 'sh', 'set {}', 'export {}={}' ]
}

##########################################################################

idp = None
envIndex = None
if len(enabledEnvs) == 1:
    try:
        envIndex = enabledEnvs[0]
        idp = IDPS[ENVS[envIndex]]
    except:
        idp = None

# something went wrong above
# or we're using the hidden option to force the menu
while idp is None:
    # check input
    try:
        print("Select an environment to log in to:")
        for k,v in enumerate(enabledEnvs):
            print('[ {} ]: {}'.format(k, ENVS[v]))
        selected = input('Selection: ')
        print()
        selected = int(selected)
        # avoid negative indices
        if selected >=0 and selected <= len(enabledEnvs):
            envIndex = enabledEnvs[selected]
            idp = IDPS[ENVS[envIndex]]
        else:
            raise IndexError
    except:
        print('You have selected an invalid environment index, please try again')
    print("Please enter your {} SSO credentials:".format(ENVS[envIndex]))

if username is None:
    username = input('Username: ')
password = getpass.getpass()

# Initiate session handler
session = requests.Session()

# Programmatically get the SAML assertion
# Opens the initial IdP url and follows all of the HTTP302 redirects, and
# gets the resulting login page
idpentryurl = idp + idpRoot
response = session.get(idpentryurl, verify=sslverification)

# Capture the idpauthformsubmiturl, which is the final url after all the redirects
idpauthformsubmiturl = response.url

# Parse the response and extract all the necessary values
# in order to build a dictionary of all of the form values the IdP expects
soup = BeautifulSoup(response.text, "html.parser")
payload = {}
for inputtag in soup.find_all(re.compile('(INPUT|input)')):
    name = inputtag.get('name','')
    value = inputtag.get('value','')
    if "user" in name.lower():
        #Make an educated guess that this is the right field for the username
        payload[name] = username
    elif "email" in name.lower():
        #Some IdPs also label the username field as 'email'
        payload[name] = username
    elif "pass" in name.lower():
        #Make an educated guess that this is the right field for the password
        payload[name] = password
    else:
        #Simply populate the parameter with the existing value (picks up hidden fields in the login form)
        payload[name] = value

for inputtag in soup.find_all(re.compile('(FORM|form)')):
    action = inputtag.get('action')
    if action:
        idpauthformsubmiturl = idp + action

# Performs the submission of the IdP login form with the above post data
response = session.post(
    idpauthformsubmiturl, data=payload, verify=sslverification)

# Incorrect credentials
if 'failed to login' in response.text.lower():
    errorMsg = 'Error: Failed to login. Please check your userid or password.'
    sys.exit(errorMsg)

# Password change prompt
if 'password change' in response.text.lower():
    errorMsg = 'Error: Password change detected. Please login via the browser to complete the password change form.'
    sys.exit(errorMsg)

# User was prompted for MFA
if 'mfa' or 'passcode' in response.text.lower():
    password = getpass.getpass('MFA Password:')
    soup = BeautifulSoup(response.text, "html.parser")
    payload = {}
    for inputtag in soup.find_all(re.compile('(INPUT|input)')):
        name = inputtag.get('name','')
        value = inputtag.get('value','')
        if "user" in name.lower():
            #Make an educated guess that this is the right field for the username
            payload[name] = username
        elif "pass" in name.lower():
            #Make an educated guess that this is the right field for the password
            payload[name] = password
        else:
            #Simply populate the parameter with the existing value (picks up hidden fields in the login form)
            payload[name] = value

    idpauthformsubmiturl = response.url
    for inputtag in soup.find_all(re.compile('(FORM|form)')):
        action = inputtag.get('action')
        if action:
            idpauthformsubmiturl = idp + action

    # Performs the submission of the IdP login form with the above post data
    response = session.post(
        idpauthformsubmiturl, data=payload, verify=sslverification)

# Overwrite and delete the credential variables, just for safety
username = '##############################################'
password = '##############################################'
del username
del password

# Decode the response and extract the SAML assertion
soup = BeautifulSoup(response.text, "html.parser")

# Look for the SAMLResponse attribute of the input tag (determined by
# analyzing the debug print(lines above)
assertion = ''
for inputtag in soup.find_all('input'):
    if inputtag.get('name') == 'SAMLResponse':
        assertion = inputtag.get('value')

# Unknown error
if assertion == '':
    #TODO: Insert valid error checking/handling
    errorMsg = 'Error: Response did not contain a valid SAML assertion!'
    errorMsg += '\nThis is likely due to an incorrect password or password expiration prompting for a new password.'
    errorMsg += '\nYou will have to log in via the browser to change your password.'
    sys.exit(errorMsg)

if opts.samlDebug:
    with open("idp-response.html", "w") as r:
        r.write(response.text)
    with open("saml-base64.txt", "w") as saml:
        saml.write(assertion)

# Parse the returned assertion and extract the authorized roles
awsroles = []
root = ET.fromstring(base64.b64decode(assertion))

if opts.samlDebug:
    with open("saml.txt", "w") as saml:
        saml.write(minidom.parseString(ET.tostring(root)).toprettyxml(indent=' '*4))

for saml2attribute in root.iter('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
    if saml2attribute.get('Name') == 'https://aws.amazon.com/SAML/Attributes/Role':
        for saml2attributevalue in saml2attribute.iter('{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
            awsroles.append(saml2attributevalue.text)

# are there any roles?
if len(awsroles) == 0:
    sys.exit('You have no AWS roles in the {} environment\n'.format(ENVS[envIndex]))

# Note the format of the attribute value should be role_arn,principal_arn
# but lots of blogs list it as principal_arn,role_arn so let's reverse
# them if needed
validRoleIndex = None
for i, awsrole in enumerate(awsroles):
    chunks = awsrole.split(',')
    if 'saml-provider' in chunks[0]:
        awsroles[i] = chunks[1] + ',' + chunks[0]
    # case insensitive role arn comparison
    if opts.role_arn is not None and opts.role_arn.upper() == awsroles[i].split(',')[0].upper():
        validRoleIndex = i

if opts.subparser_name == 'list-roles':
    list_roles(opts, awsroles)
    sys.exit(0)

if opts.role_arn is not None and validRoleIndex is None:
    print("Warning: Could not find selected role {}".format(opts.role_arn))

# Only one role available so auto-select it
if len(awsroles) == 1:
    validRoleIndex = 0

try:
    role_arn = awsroles[validRoleIndex].split(',')[0]
    principal_arn = awsroles[validRoleIndex].split(',')[1]

# If I have more than one role, or something went wrong above,
# ask the user which one they want
except:
    # sort the roles so they are in account number then role name order
    sp = len(str(len(awsroles)))
    awsroles.sort()
    role_arn = None
    while role_arn is None:
        try:
            acctNum = None
            print("Please choose the AWS role you would like to assume:")
            for i, awsrole in enumerate(awsroles):
                arn = awsrole.split(',')[0]
                acct = arn.split(':')[4]
                role = arn.split(':')[-1]
                if acctNum != acct:
                    acctNum = acct
                    print(sep)
                    print('Account {}'.format(acctNum))
                    print(sep)
                print('[ {i:>{sp}d} ]: {arn}'.format(sp=sp, i=i+1, arn=role))
            print(sep)
            selected = input("Selection: ")
            print()
            index = int(selected) - 1
            if index >=0 and index <= len(awsroles):
                role_arn = awsroles[index].split(',')[0]
                principal_arn = awsroles[index].split(',')[1]
            else:
                raise IndexError
        except:
            print('You selected an invalid role index, please try again')

if not opts.role_arn:
    print('Selected role: {}'.format(role_arn))

# Use the assertion to get an AWS STS token using Assume Role with SAML
try:
    # use explicit region and false credentials
    # forcing boto to ignore the .aws/config adnd .aws/credentials files
    client = boto3.client('sts', region_name=region,
        aws_access_key_id="ABCD",
        aws_secret_access_key="ABCD")
    parameters = { 'RoleArn': role_arn, 'PrincipalArn': principal_arn, 'SAMLAssertion': assertion }
    token = client.assume_role_with_saml(**parameters)
except:
    sys.exit('Error: Failed to assume role.')

# collect the sts token information
aws_access_key_id = token['Credentials']['AccessKeyId']
aws_secret_access_key = token['Credentials']['SecretAccessKey']
aws_session_token = token['Credentials']['SessionToken']
if opts.subparser_name == 'credential-helper':
    expiration = token['Credentials']['Expiration'].isoformat()
else:
    expiration = token['Credentials']['Expiration'].astimezone(tzlocal())

if opts.subparser_name == 'credential-helper':
    obj = {
        'Version': 1,
        'AccessKeyId': aws_access_key_id,
        'SecretAccessKey': aws_secret_access_key,
        'SessionToken': aws_session_token,
        'Expiration': expiration
    }
    print(json.dumps(obj, indent=4))
    sys.exit(0)

if not opts.role_arn:
    print('Successfully assumed role: {}'.format(role_arn))

# check for the existance of the .aws folder
profile = opts.profile
if not os.path.exists(awsDir):
    try:
        os.makedirs(awsDir)
        print('\nWarning: Created the missing {} folder.\nThe AWS CLI may not be installed, or may not have been configured.'.format(awsDir))
    except:
        sys.exit('Error: Could not create the missing {} folder.'.format(awsDir))

# write the sts token to the ~/.aws/credentials file
try:
    # Read in the existing config file
    config = configparser.ConfigParser()
    config.read(credFile)

    # check for the presence of the profile in ~/.aws/config?
    if not config.has_section(profile):
        config.add_section(profile)
    for key in [ "aws_access_key_id", "aws_secret_access_key", "aws_session_token" ]:
        config[profile][key] = vars()[key]

    # Write the updated config file
    with open(credFile, 'w') as configfile:
        config.write(configfile)

except:
    sys.exit('Error: Failed to write to credentials file, {}'.format(filename))

# Give the user some basic info as to what has just happened
try:
    # output temporary identity for the user
    # this must use the new credentials!
    client = boto3.client('sts', region_name=region,
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        aws_session_token=aws_session_token)
    identity = client.get_caller_identity()
    del identity['ResponseMetadata']
    print()
    print(json.dumps(identity, sort_keys=True, indent=4))
    print()
except:
    pass

print(sep)
print('Your new access key has been stored in the [{}] profile'.format(profile))
print('in configuration file {}.'.format(credFile))
print('It will expire at {} {}.'.format(expiration, expiration.tzname()))
print('You may safely rerun this script to refresh your access key.')
#print('\nBe sure to set your proxy as follows before using the AWS CLI,')
#proxy='HTTPS_PROXY={proxy}'.format(proxy=proxy)
#print("set ",proxy)
print(sep)
print()

# export AWS_* environment variables to parent shell
ext = OUTPUT[parentShell][0]
filename = os.path.join(home, '.aws', 'env.' + ext)
try:
    with open(filename, "w") as out:
        strFormat = OUTPUT[parentShell][1] + '\n'
        out.write(strFormat.format('echo off'))
        for key in [ "aws_access_key_id", "aws_secret_access_key", "aws_session_token" ]:
            strFormat = OUTPUT[parentShell][2] + '\n'
            out.write(strFormat.format(key.upper(), vars()[key]))
except:
    sys.exit("Error: Unable to write environment vars to {}".format(filename))

sys.exit(0)

