package test

import (
	"fmt"
	"testing"
        // "os"
        //"strings"

	awsSDK "github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// An example of how to test the Terraform module in examples/terraform-aws-dynamodb-example using Terratest.
func TestTerraformAwsDynamoDBExample(t *testing.T) {
	t.Parallel()

	// Pick a random AWS region to test in. This helps ensure your code works in all regions.
	awsRegion := "us-east-1"
        tfStateFileDir    := "../../examples/dynamodb"
        // tfStateFileDir := os.Getenv("TFSTATEFILE")

	// Set up expected values to be checked later
	expectedTableName := fmt.Sprintf("pipeline-test-%s", random.UniqueId())
	expectedKmsKeyArn := aws.GetCmkArn(t, awsRegion, "alias/aws-mission-control")
	expectedKeySchema := []*dynamodb.KeySchemaElement{
		{AttributeName: awsSDK.String("AccountID"), KeyType: awsSDK.String("HASH")},
		{AttributeName: awsSDK.String("JoinedTimestamp"), KeyType: awsSDK.String("RANGE")},
	}
	/* expectedTags := []*dynamodb.Tag{
		     {Key: awsSDK.String("appinstid"), Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("tenantcsiappid"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("cartid"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("ctcversionidplatform"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("ctcversionidos"), Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("ctcversionidmw"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("ctcversioniddb"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("billingprofileid"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("csiappid"), Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("environment"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("sectorspecificgoc"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},
                     {Key: awsSDK.String("sector"),Value: fmt.Sprintf("%s", strings.ToLower(random.UniqueId()))},


	 } */

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: tfStateFileDir,

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"table_name": expectedTableName,
		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Look up the DynamoDB table by name
	table := aws.GetDynamoDBTable(t, awsRegion, expectedTableName)

	assert.Equal(t, "ACTIVE", awsSDK.StringValue(table.TableStatus))
	assert.ElementsMatch(t, expectedKeySchema, table.KeySchema)

	// Verify server-side encryption configuration
	assert.Equal(t, expectedKmsKeyArn, awsSDK.StringValue(table.SSEDescription.KMSMasterKeyArn))
	assert.Equal(t, "ENABLED", awsSDK.StringValue(table.SSEDescription.Status))
	assert.Equal(t, "KMS", awsSDK.StringValue(table.SSEDescription.SSEType))

	// Verify TTL configuration
	ttl := aws.GetDynamoDBTableTimeToLive(t, awsRegion, expectedTableName)
	assert.Equal(t, "ttl", awsSDK.StringValue(ttl.AttributeName))
	assert.Equal(t, "ENABLED", awsSDK.StringValue(ttl.TimeToLiveStatus))

	 // Verify resource tags
	/* tags := aws.GetDynamoDbTableTags(t, awsRegion, expectedTableName)
	assert.ElementsMatch(t, expectedTags, tags) */
}
