
provider "aws" {
  alias  = "replica"
  region = var.replica_region
}


data "aws_kms_key" "by_alias" {
  key_id = "alias/aws-mission-control"
}

data "aws_kms_key" "by_alias_replica" {
  key_id   = "alias/aws-mission-control"
  provider = aws.replica
}



resource "aws_dynamodb_table" "dynamodb_table" {
  name             = var.table_name
  billing_mode     = var.billing_mode
  read_capacity    = var.billing_mode != "PAY_PER_REQUEST" ? var.read_capacity : null
  write_capacity   = var.billing_mode != "PAY_PER_REQUEST" ? var.write_capacity : null
  hash_key         = var.hash_key_value
  range_key        = var.range_key_value != "" ? var.range_key_value : ""
  stream_enabled   = var.stream_enabled_status
  stream_view_type = var.stream_view_type_value

  point_in_time_recovery {
    enabled = var.point_in_time_recovery_status
  }

  dynamic "attribute" {
    for_each = var.attribute_index
    content {
      name = attribute.value["name"]
      type = attribute.value["type"]
    }
  }

  ttl {
    attribute_name = var.ttl_attribute_name
    enabled        = var.ttl_enabled_status
  }

  server_side_encryption {
    enabled     = true
    kms_key_arn = data.aws_kms_key.by_alias.arn
  }

  dynamic "global_secondary_index" {
    for_each = var.gs_index
    content {
      name               = global_secondary_index.value["name"]
      hash_key           = global_secondary_index.value["hash_key"]
      range_key          = global_secondary_index.value["range_key"]
      write_capacity     = global_secondary_index.value["write_capacity"]
      read_capacity      = global_secondary_index.value["read_capacity"]
      projection_type    = global_secondary_index.value["projection_type"]
      non_key_attributes = global_secondary_index.value["non_key_attributes"]
    }
  }

  dynamic "local_secondary_index" {
    for_each = var.ls_index
    content {
      name               = local_secondary_index.value["name"]
      range_key          = local_secondary_index.value["range_key"]
      non_key_attributes = local_secondary_index.value["non_key_attributes"]
      projection_type    = local_secondary_index.value["projection_type"]
    }
  }

  tags = merge(var.tags, local.pipeline_tags)
}

resource "null_resource" "global_table" {
  triggers = {
    recreate_global_table = var.recreate_global_table
  }
  depends_on = [aws_dynamodb_table.dynamodb_table]
  count      = var.create_global_table ? 1 : 0
  provisioner "local-exec" {
    command = "export AWS_DEFAULT_REGION=${var.aws_region}; export RECREATE_GLOBAL_TABLE=${self.triggers.recreate_global_table}; aws dynamodb update-table --table-name ${var.table_name} --replica-updates '[ { \"Create\": { \"RegionName\": \"${var.replica_region}\", \"KMSMasterKeyId\": \"${data.aws_kms_key.by_alias_replica.arn}\" } } ]'"
  }
}

