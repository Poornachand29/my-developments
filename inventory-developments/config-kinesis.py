import json
import boto3
import botocore
import botocore.exceptions
from botocore.exceptions import ClientError
import base64
import json
import os
import datetime
import time
#aggregator_name = os.environ['AGGREGATOR_NAME']
AGGREGATOR_NAME = "Inventory-Account-Aggregator"
config_client = boto3.client('config',region_name = 'us-east-1')
kinesis_client = boto3.client('kinesis',region_name = 'us-east-1')


def lambda_handler(event, context):
    # TODO implement
    print(event)
    resource_type = event['resource_type']
    resources_list = event['resources_list']
    for resource in resources_list:
        fetch_resource = fetch_resource_info(resource_type,resource)
        print(json.dumps(fetch_resource, default = myconverter))
        kinesis_write = write_to_kinesis(fetch_resource)
        print(kinesis_write)





def fetch_resource_info(resource_type,resource):
    try:
        response = config_client.get_aggregate_resource_config(ConfigurationAggregatorName=aggregator_name,
                                                                ResourceIdentifier={
                                                                'SourceAccountId': resource['SourceAccountId'],
                                                                'SourceRegion': resource['SourceRegion'],
                                                                'ResourceId': resource['ResourceId'],
                                                                'ResourceType': resource_type })
    except ClientError as error:
        print(error)
        raise error
        
    return response


def write_to_kinesis(fetch_resource):
    try:
        encode_fetch = json.dumps(fetch_resource, default = myconverter)
        kinesis_response = kinesis_client.put_records(
            Records=[
                {
                    #'Data': encode_fetch_ec2,
                    'Data': base64.b64encode(encode_fetch.encode('utf-8')),
                    'PartitionKey': fetch_resource['ConfigurationItem']['configurationStateId']
                },
            ],
        StreamName='inventory-aggregator-snapshot-test'
        )
    except ClientError as error:
        print(error)
        raise error
        
    return kinesis_response
    
def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()
