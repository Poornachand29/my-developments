import boto3
import yaml
import json
import re

client = boto3.client('s3')
config = boto3.client('config')



def lambda_handler(event, context):
    # TODO implement
    data = client.get_object(Bucket='tags-bucket-123', Key='golden-source-tags.yaml')
    contents = data['Body'].read()
    print(type(contents)) 
    with open('/tmp/tags.yaml', 'wb') as f:
        client.download_fileobj('tags-bucket-123', 'golden-source-tags.yaml', f)

    mandatory_tags =  []
    scope = []
    missing_tags = []
    with open('/tmp/tags.yaml') as f:
    
        docs = yaml.load(f, Loader=yaml.FullLoader)
        #docs = yaml.safe_load(f)
    print(type(docs))
    #for item, doc in docs.items():
    #    print(item, ":", doc)
 
    list_tags = docs["Tags"]
    for i in range(len(list_tags)):
        print(list_tags[i]['tag'])
        print(list_tags[i]['ismandatory'])
        if ( list_tags[i]['ismandatory'] == True ):
            #print("Hello")
            mandatory_tags.append(list_tags[i]['tag'])
        scope = list_tags[i]['scope']
        print("scope of %s are %s"%(list_tags[i]['tag'],str(scope)))
        result = re.search('::(.*)::', scope[0])
        if(result.group(1)[0] != "*"):
            print(scope[0][5:16])
    print("List of manadatory tags:", mandatory_tags)
    response = config.describe_config_rules(
        ConfigRuleNames=[
            'config-tag-validation',
        ],
    )
    manadatory_tags_config = response['ConfigRules'][0]['InputParameters']
    y = json.loads(manadatory_tags_config)

 
    config_tags = y["MandatoryTags"]
    list_config_tag = list(config_tags.split(",")) 
    print("List of tags from config rule:", list_config_tag)
    for i in mandatory_tags:
        if i not in list_config_tag:
            missing_tags.append(i)
    print("Missing tags from config rule:", missing_tags)

   