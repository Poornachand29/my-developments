import os
#import xlsxwriter
import csv
import boto3
from botocore.client import Config
s31 = boto3.resource('s3')
# Get the service client with sigv4 configured valid for 7 days
s32 = boto3.client('s3', config=Config(signature_version='s3v4'))
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication


def lambda_handler(event, context):
    user_key = ['username:' , 'user', 'acc_key:' , 'access_key', 'sec_key:' , 'secret_key' ]
    print(user_key)
    print(type(user_key))
            
    with open('/tmp/mylist.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(user_key)
    
    s31.meta.client.upload_file('/tmp/mylist.csv', 'presignedurl123', 'mylist.csv')
    
    #presigned_url = s3.generate_presigned_url('get_object', Params = {'Bucket': 'www.mybucket.com', 'Key': 'hello.txt'}, ExpiresIn = 100)
    
    presigned_url = s32.generate_presigned_url(
    ClientMethod='get_object',
    Params={
        'Bucket': 'presignedurl123',
        'Key': 'mylist.csv'
    },
    ExpiresIn=604800
    )
    
    print(presigned_url)
   
    # Replace sender@example.com with your "From" address.
    # This address must be verified with Amazon SES.
    SENDER = "poornachand29@gmail.com"

    # Replace recipient@example.com with a "To" address. If your account 
    # is still in the sandbox, this address must be verified.
    RECIPIENT = "poornachand81@gmail.com"

    # Specify a configuration set. If you do not want to use a configuration
    # set, comment the following variable, and the 
    # ConfigurationSetName=CONFIGURATION_SET argument below.
    CONFIGURATION_SET = "ConfigSet"

    # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
    AWS_REGION = "us-east-1"

    # The subject line for the email.
    SUBJECT = "Presigned url test"

    # The email body for recipients with non-HTML email clients.
    #BODY_TEXT = presigned_url
    BODY_TEXT = ("Amazon SES Test (Python)\r\n"
                 "This email was sent with Amazon SES using the "
                 "AWS SDK for Python (Boto)."
                )
    
    print(presigned_url)
            
    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
    azon SES Test (SDK for Python)</h1>
        <p>This email was sent with
            <a href='{code}'>presigned_url</a> using the
            <a href='https://aws.amazon.com/sdk-for-python/'>
            AWS SDK for Python (Boto)</a>.</p>
    </body>
    </html>
            """.format(code=presigned_url)            

    # The character encoding for the email.
    CHARSET = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses',region_name=AWS_REGION)

    # Try to send the email.
    try:
        #Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER
            # If you are not using a configuration set, comment or delete the
            # following line
            #ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
        
    