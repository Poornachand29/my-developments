#import json
import boto3
import os
import sys
import csv
#import chilkat
from datetime import datetime, timezone
#from django.utils import timezone
import datetime
import dateutil
from botocore.client import Config
s31 = boto3.resource('s3')
# Get the service client with sigv4 configured valid for 7 days
s32 = boto3.client('s3', config=Config(signature_version='s3v4'))
iam_client = boto3.client('iam')
client = boto3.client('sns')
from zipfile36 import ZipFile
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    #users = os.environ['user_names']
    #users list to rotate IAM keys
    users = ['test2']
    #users = [users]
    print(type(users))
    for user in users:
        inactive_keys = 0
        active_keys = 0
        keys = iam_client.list_access_keys(UserName=user)
        print(keys)
        access_key_id_list=[]
        access_key_last_used_date_list=[]
        for key in keys['AccessKeyMetadata']:
            print(key['Status'])
            access_key_id = key['AccessKeyId']
            access_key_last_used = iam_client.get_access_key_last_used(
            AccessKeyId=access_key_id
            )
            access_key_last_used_date = access_key_last_used['AccessKeyLastUsed']['LastUsedDate']
            #print(type(access_key_last_used_date))
            print(access_key_last_used_date)
            #access_key_last_used_date_time = access_key_last_used_date['LastUsedDate']
            createddate = key['CreateDate']
            print("createddate:")
            print(createddate)
            todaysdate = datetime.datetime.now(timezone.utc)
            print("todays_date:")
            print(todaysdate)
            key_maxdays=0
            #subtract maximum days from todays date
            timelimit_date=datetime.datetime.now(timezone.utc) - datetime.timedelta(days=key_maxdays)
            print("timelimit_date:")
            print(timelimit_date)
            access_key_id_list.append(access_key_id)
            access_key_last_used_date_list.append(access_key_last_used_date)
            #for x in access_key_id:
            #    access_key_id_list.append(foo(x))
            if key['Status']=='Inactive': inactive_keys = inactive_keys + 1
            elif key['Status']=='Active': active_keys = active_keys + 1
        print(access_key_id_list)
        print(access_key_last_used_date_list)
        print("%s has %d inactive keys and %d active keys" % (user, inactive_keys, active_keys))
        #check for the number of keys
        max_value = max(access_key_last_used_date_list)
        max_index = access_key_last_used_date_list.index(max_value)
        max_access_key_id = access_key_id_list[max_index]
        if inactive_keys + active_keys >= 2:
            min_value = min(access_key_last_used_date_list)
            print(min_value)
            min_index = access_key_last_used_date_list.index(min_value)
            print(min_index)
            min_access_key_id = access_key_id_list[min_index]
            print(min_access_key_id)
            response = iam_client.delete_access_key(
            UserName=user,
            AccessKeyId=min_access_key_id
            )
        print("timelimit_date:")
        print(timelimit_date)
        print("createddate:")
        print(createddate)
        #create access key if no of keys are less than 2 and the given timelimit of the access key expired 
        if (timelimit_date >= createddate):
            access_key_metadata = iam_client.create_access_key(UserName=user)['AccessKey']
            access_key = access_key_metadata['AccessKeyId']
            secret_key = access_key_metadata['SecretAccessKey']
            print(access_key,secret_key)
            
            print(max_access_key_id)
            account_id = boto3.client('sts').get_caller_identity().get('Account')
            print(account_id)
            user_key = ['account_id:' , account_id , 'username:' , user, 'old_access_key:' , max_access_key_id , 'new_access_key:' , access_key, 'new_secret_key:' , secret_key ]
            print(user_key)
            print(type(user_key))
            
            #Create csv file and write the user key credentials
            with open('/tmp/mylist.csv', 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(user_key)
                
            with ZipFile('/tmp/spam.zip', 'w') as myzip:
				myzip.write('/tmp/mylist.csv')
            
            #Upload the csv in restricted s3 bucket
            s31.meta.client.upload_file('/tmp/spam.zip', 'presignedurl123', 'spam.zip')
    
            #presigned_url = s3.generate_presigned_url('get_object', Params = {'Bucket': 'www.mybucket.com', 'Key': 'hello.txt'}, ExpiresIn = 100)
    
            #Create presigned url for s3 bucket file
            presigned_url = s32.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': 'presignedurl123',
                'Key': 'spam.zip'
            },
            ExpiresIn=604800
            )
    
            print(presigned_url)
            
            #Publish the url to the user through SNS through which user can download the file
            response = client.publish(
            TopicArn='arn:aws:sns:us-east-1:295263659647:test',
            Message=presigned_url,
            Subject="access_keys of %s" % (user)
            )
        else:
            print ("%s user's access key date not yet expired" % (user))

    
            
    
            