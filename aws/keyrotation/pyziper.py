import pyzipper

secret_password = b'lost art of keeping a secret'

user_key =  ['_key' , 'list' ]
user_key1 = str(user_key)[1:-1].replace("'","")

with pyzipper.AESZipFile('new_test.zip',
                         'w',
                         compression=pyzipper.ZIP_LZMA,
                         encryption=pyzipper.WZ_AES) as zf:
    zf.pwd = secret_password
    zf.writestr('test.csv', user_key1 )
	#zf.writestr('test.csv', "user_key")
	

with pyzipper.AESZipFile('new_test.zip') as zf:
    zf.pwd = secret_password
    my_secrets = zf.read('test.csv')
