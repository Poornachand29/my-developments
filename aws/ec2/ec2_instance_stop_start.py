#!/usr/bin/python

import boto3
import sys
import os

try:
    Opt = sys.argv[1]

    region = 'us-east-1'
    #instances = ['i-12345cb6de4f78g9h', 'i-08ce9b2d7eccf6d26']
    instances = ['i-079033a84c1c3fd3b']
    ec2 = boto3.client('ec2', region_name=region)

    if (Opt == "stop"):
       try:
           ec2.stop_instances(InstanceIds=instances)
           print('stopped your instances: ' + str(instances))
       except:
           print('The instance %s is not in a state from which it can be stopped.' %instances)
    elif (Opt == "start"):
       try:
           ec2.start_instances(InstanceIds=instances)
           print('started your instances: ' + str(instances))
       except:
           print('The instance %s is not in a state from which it can be started.' %instances)
    else:
       print("%s <start / stop>" %sys.argv[0])
except IndexError:
    print("%s <start / stop>" %sys.argv[0])
