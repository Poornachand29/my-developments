#!/usr/bin/python

import boto3
import time

client = boto3.client('ec2', region_name='us-east-2')
ec2 = boto3.resource('ec2')

instance_name = "win-git-app"

user_data = '''<script>
mkdir c:\gitrepo
mkdir c:\gitrepo\myrepo
mkdir c:\opt
cd c:\gitrepo

echo [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 > download_git.ps1
echo $down = New-Object System.Net.WebClient >> download_git.ps1
echo $url = 'https://bitbucket.org/mprukmangathan/powershell/get/82afe4343892.zip' >> download_git.ps1
echo $file = 'C:\gitrepo\myrepo.zip' >> download_git.ps1
echo $down.DownloadFile($url,$file) >> download_git.ps1
echo $exec = New-Object -com shell.application >> download_git.ps1
echo $destination = 'C:\gitrepo\myrepo'  >> download_git.ps1
echo Expand-Archive -LiteralPath $file -DestinationPath $destination >> download_git.ps1
echo exit >> download_git.ps1

powershell "& ""C:\gitrepo\download_git.ps1"""
powershell "& ""C:\gitrepo\myrepo\*\software\install.ps1"""

net session >nul 2>&1
if %errorLevel% == 0 (
    @echo on
    echo process successfull > execution_logs.txt
    shutdown /s
) else (
    @echo on
    echo process unsuccessfull  > execution_logs.txt
    @echo off
    exit /b 0
)
</script>'''

response = client.run_instances(
    BlockDeviceMappings=[
        {
            'DeviceName': '/dev/xvda',
            'Ebs': {

                'DeleteOnTermination': True,
                'VolumeSize': 8,
                'VolumeType': 'gp2'
            },
        },
    ],
    ImageId='ami-0587770c46124f677',
    InstanceType='t2.micro',
    KeyName='ruk2020',
    MaxCount=1,
    MinCount=1,
    Monitoring={
        'Enabled': False
    },
    SecurityGroupIds=[
        'sg-04a97a6bb4cc46013',
    ],
    TagSpecifications=[ 
        { 
             'ResourceType': 'instance',
             'Tags': [
                {
                    'Key': 'Name',
                    'Value': instance_name
                },
             ]
        },
    ],
    UserData = user_data
)

time.sleep(180)

for instance in ec2.instances.all():
    if instance.state['Name'] == "stopped":
        for tag in instance.tags:
            if tag['Value'] == instance_name:
               ec2_instance_id = instance.id
               client.create_image(InstanceId=ec2_instance_id,Name=instance_name,NoReboot=True)
    else:
        print ("instance %s still running, so there might be some issue with execution" %instance_name)
        exit (1)
