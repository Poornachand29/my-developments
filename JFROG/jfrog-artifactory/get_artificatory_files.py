#!/usr/bin/python

import os
import requests
from bs4 import BeautifulSoup
import datetime

Current_Date = datetime.datetime.today().strftime ('%d-%b-%Y')
print ('Current Date: ' + str(Current_Date))

now = datetime.datetime.now()
current_time = now.strftime("%H:%M")
print("Current Time =", current_time)

Ten_minutes = datetime.timedelta(minutes=10)
Validate_time = now - Ten_minutes
tem_min_before = Validate_time.strftime("%H:%M")
print ("ten minutes before =", tem_min_before)

ip = os.environ['elasticip']
url = "http://" + ip + ":8082/artifactory/generic-local/"
response = requests.get(url, auth=('admin','admin@123'))
soup = BeautifulSoup(response.text, 'html.parser')

Mylist = soup.find_all('pre')
Mylist.pop(0)

for element in Mylist:
    Mydata = str(element)
    Newlist = Mydata.split("\n")
    for item in Newlist:
        try:
            nums = item.split("</a>",1)[1]
            data = nums.split()
            #print (type(data[0]))
            if Current_Date == data[0]: 
               print ("date matches - %s-%s" %(data[0],data[1]))
               if ((data[1] >= tem_min_before) and (data[1] <= current_time)):
                  print ("%s >= %s and %s <= %s" %(data[1],tem_min_before,data[1],current_time))
               else:
                  print ("no files found for last 10 minutes")
            else:
               print ("date not matches - %s-%s" %(data[0],data[1]))
        except IndexError:
            print ("")
