################
# Create s3 bucket
################
locals {
  common_tags = {
    "cartid"               = var.cartid_tag
    "softwareaudit"        = var.softwareaudit_tag
    "billingprofileid"     = var.billingprofileid_tag
    "citisystemsinventory" = var.citisystemsinventory_tag
    "Environment"          = var.Environment
    "SectorSpecificGOC"    = var.SectorSpecificGOC
  }
}

resource "aws_s3_bucket" "this_s3" {
  bucket        = var.bucket_name
  acl           = var.public ? "public-read" : "private"
  force_destroy = var.force_destroy
  policy        = var.bucket_policy != "" ? var.bucket_policy : ""
  tags          = merge(var.tags, local.common_tags)

  versioning {
    enabled = var.versioning
  }
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket = aws_s3_bucket.this_s3.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
