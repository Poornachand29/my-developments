resource "aws_dynamodb_table" "config_aggregator" {
  name           = "cmdb-config-aggregator"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "accountid"
  range_key      = "date"

  attribute {
    name = "accountid"
    type = "N"
  }

  attribute {
    name = "date"
    type = "S"
  }
   
  server_side_encryption {
    enabled = true
    kms_key_arn = aws_kms_key.mykmskey.arn
  }
  
  tags = {
    Name        = "cmdb-config-aggregator"
    Environment = "Development"
  }
}
