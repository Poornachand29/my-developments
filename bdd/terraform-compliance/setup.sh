#!/bin/bash

cd /tmp

# Install terraform
wget https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip
unzip terraform_0.12.28_linux_amd64.zip
sudo mv terraform /usr/bin/

# Install terraform compliance
sudo pip install terraform-compliance
