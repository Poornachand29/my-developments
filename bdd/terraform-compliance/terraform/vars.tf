variable region {
    default = "us-east-1"
} 

variable deletion_window_in_days {
   default = 10
}

variable retention_in_days {
   default = 14
}

variable read_capacity {
    default = 20
}

variable write_capacity {
    default = 20
}

variable lambda_tags {
    type    = map
    default = {
         Name = "test_lambda"
         Environment = "Development"
    }
}

variable dynamodb_tags {
    type    = map
    default = {
         Name = "cmdb-config-aggregator"
         Environment = "Development"
    }
}
