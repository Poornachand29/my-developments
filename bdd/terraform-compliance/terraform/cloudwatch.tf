resource "aws_cloudwatch_log_group" "cw_log_group" {
  name              = "/aws/lambda/test_lambda"
  retention_in_days = var.retention_in_days
}
