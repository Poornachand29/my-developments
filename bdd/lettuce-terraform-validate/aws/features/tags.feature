Feature: Resources should be properly tagged

  Scenario Outline: Name tag
    Given I have terraform configuration at "aws/terraform"
    When I define a resource that supports tags
    Then it must contain <tags>
    And its value must be set by a variable
        Examples:
                | tags        |
                | Name        |
                | Environment |
