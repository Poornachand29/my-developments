from difflib import get_close_matches


def close_match( columna, columnblist ):
    return get_close_matches( columna, columnblist, 1, 0.6 )

output = close_match('appel', ['ape', 'apple', 'peach', 'puppy'])
print(output)


