from ctrl4bi import cleanser
from ctrl4bi import etl_testing
from ctrl4bi import datasets
import pandas as pd
import pymysql
import pypyodbc as pydb
from difflib import get_close_matches
import time
host="test.cr17hxipyxa2.us-east-1.rds.amazonaws.com"
port=1433
dbname="NewDB"
user="admin"
password="keyprowler1!"

#try:
#    conn = pymysql.connect(host, user=user,port=port, passwd=password, db=dbname)
#except Exception as e:
#    print(e)
try:
    conn = pydb.connect('DRIVER={SQL Server};PORT=1433;SERVER='+host+';DATABASE='+dbname+';uid='+user+';pwd='+ password)
except Exception as e:
    print(e)
print(conn)
time.sleep(3)
sql1 = """
SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Persons'
"""
try:
    table1_columns = pd.read_sql(sql1, con=conn)
except Exception as e:
    print(e)

print(table1_columns)
print(type(table1_columns))

for column in table1_columns.columns:

    # Storing the rows of a column
    # into a temporary list
    lis1 = table1_columns[column].tolist()

    print(lis1)

sql2 = """
SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Persons_new'
"""

try:
    table2_columns = pd.read_sql(sql2, con=conn)
except Exception as e:
    print(e)

print(table2_columns)

for column in table2_columns.columns:

    # Storing the rows of a column
    # into a temporary list
    lis2 = table2_columns[column].tolist()

    print(lis2)

sorted_lis2 = []
matcher={}
for col in lis1:
    def close_match( columna, columnblist ):
        return get_close_matches( columna, columnblist, 1, 0.6 )

    output = close_match(col, lis2)
    print(output[0])
    matcher[col] = output[0]
    sorted_lis2.append(output[0])

print(sorted_lis2)

#cursor = conn.cursor()
target_query='SELECT '
source_query='SELECT '


for each_col in matcher.keys():
    val=matcher[each_col]
    alias_str=val+' as '+each_col+', '
    target_query+=alias_str
    source_query+=each_col+', '

source_size = len(source_query)
target_size = len(target_query)

mod_source_query = source_query[:source_size - 2]
mod_target_query = target_query[:target_size - 2]

print(mod_source_query)
print(mod_target_query)

mod_target_query+=' FROM Persons_new'
mod_source_query+=' FROM Persons'

print(mod_source_query)
print(mod_target_query)

df1=pd.read_sql(mod_source_query,conn)
df2=pd.read_sql(mod_target_query,conn)

print("src_df",df1)
print("tgt_df",df2)
mismatch_count,logs,mismatch_df =etl_testing.column_level_check(df1,df2,'PersonID')
print('\n'.join(logs))
print(mismatch_df)
print(mismatch_count)

