#!/usr/bin/env python
# Mariusz Kasinski <mariusz.kasinski@citi.com>
"""This script helps to update all Citi mandatory tags in selected AWS account"""

import argparse
import re
import boto3

# mandatory tags table:
#   directory key   - tag name
#   directory value - argparse switch
# if one of the tags is no longer mandatory - remove line related to this. 
# Just make sure that last line in the list is not finished with comma
# If one more tag is required - add the line + corresponding letter.
# Remember that letter must be unique and it cannot be "-l", "-p", "-i" or "-r"
mandatoryTags = {
        "application_id"        : "a",
        "app_inst_id"           : "d",
        "chargeback_profile_id" : "c",
        "cloud_support_group"   : "s",
        "os_name"               : "n",
        "os_version"            : "v",
        "os_patchlevel"         : "t"
    }

def update_mandatory_ec2_tags(ec2_client, ec2_region, exec_args):
    """The function updates mandatory tags in the EC2 instances"""
    print("Region: {}".format(ec2_region))

    paginator = ec2_client.get_paginator("describe_instances")
    instances = []
    if exec_args.instances == ["all"]:
        page_iterator = paginator.paginate()
        for page in page_iterator:
            for resource in page['Reservations']:
                instances.append(resource['Instances'][0]['InstanceId'])
    else:
        instances = exec_args.instances

    mtags=[]
    for tag in mandatoryTags:
        mtags.append({ 'Key': tag, 'Value': getattr(exec_args, tag)})
    ec2_client.create_tags(Resources=instances, Tags=mtags)
    for i in instances:
        print("Updated mandatory tags in {} ".format(i))

def list_ec2_instances(ec2_client, ec2_region, ec2_instances):
    """Function which pulls all or selected list of instances from
        the existing boto3 Session client"""
    mtags       = {}
    ntags       = {}
    used_amis   = []
    ISFIRST     = True

    paginator = ec2_client.get_paginator("describe_instances")
    page_iterator = ""
    instances = []
    if ec2_instances == ["all"]:
        page_iterator = paginator.paginate()
    else:
        page_iterator = paginator.paginate(InstanceIds=instances)

    for page in page_iterator:
        for resource in page['Reservations']:
            if ISFIRST:
                print("List of EC2 instances in region: {}".format(ec2_region))
                ISFIRST = False
            i = resource['Instances'][0]
            if not i['ImageId'] in used_amis:
                used_amis.append(i['ImageId'])
            ntags = {}
            for tag in mandatoryTags:
                mtags[tag] = "Null"

            if 'Tags' in i:
                for tag in i['Tags']:
                    if tag['Key'] in mtags:
                        mtags[tag['Key']]=tag['Value']
                    else:
                        ntags[tag['Key']]=tag['Value']
            ### formatted list of instance Ids with focus on mandatory tags and not formatted string
            ### presenting non-mandatory tags (as not key information which still can help to double
            ### check if potential tag value was provided to the other, incorrect tag key)
            disp_string = "{}, AMI: {}".format(i['InstanceId'],i['ImageId'])
            for tag, tvalue in mtags.items():
                disp_string += ", {}: {}".format(tag, tvalue)
            for tag, tvalue in ntags.items():
                disp_string += ", {}: {}".format(tag, tvalue)
            print("{}".format(disp_string))

    if ISFIRST:
        print("No EC2 instances in region: {}".format(ec2_region))

    if len(used_amis) > 0:
        print("\nAMI images used by the listed above EC2 instances:")
        ami_details = ec2_client.describe_images(ImageIds=used_amis)
        for ami in ami_details['Images']:
            tags=""
            if 'Tags' in ami:
                tags=", Tags: {}".format(str(ami['Tags']))
            print("AmiID: {}, Name: {}, OwnerId: {}, Creation Date: {}, Description: {}{}".format(
                    ami['ImageId'], ami['Name'], ami['OwnerId'], ami['CreationDate'], ami['Description'], tags))
        print("\n")

##### MAIN ###########

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scripts supports updating EC2 mandatory tags')
    parser.add_argument("-i","--instances",nargs="+",type=str,metavar="instance",default="all",
        required=True,
        help="\"all\" as the only value to this option if you wish to update mandatory tags in "
            + "all EC2 instances in specified region or list of EC2 instances in which you wish"
            + "to update mandatory tags")
    parser.add_argument('-r','--regions',nargs="+",type=str,metavar='region',required=False,
        default=['us-east-1','us-east-2'],help='AWS regions')
    parser.add_argument('-p','--profile',type=str,metavar='profile',required=False,
        default="default", help='AWS profile name')
    parser.add_argument("-l","--listEC2instances",action='store_true',
        help="List all EC2 instance IDs and their tags in the AWS account and region")
    for key, value in mandatoryTags.items():
        parser.add_argument('-'+value,'--'+key,type=str,metavar='tag_value',required=False,
            help='Mandatory tag '+key)
    args = parser.parse_args()

# InstanceID validation
if ( len(args.instances) > 1 ) or ( args.instances[0].lower() != "all" ) :
    ISWRONG=False
    for ins in args.instances:
        if not re.match(r"^i-[a-z0-9]+$",ins):
            ISWRONG=True
            print("Incorrect InstanceID name: {}".format(ins))
    if ISWRONG:
        print("If this function should be executed for all EC2 instances in this account "
            + "and this region then the only argument to option \"-i\" must be \"all\". Otherwise "
            + "valid instanceIDs must be provided matching \"i-_id_\" naming convention")
# End of instanceID validation

# simple test if role can be assumed
try:
    session = boto3.Session(profile_name=args.profile, region_name=args.regions[0])
    session.client('sts').get_caller_identity().get('Account')
except:
    print("There is no access to this account using profile {}".format(args.profile))
    exit(-1)

for region in args.regions:
    session = boto3.Session(profile_name=args.profile, region_name=region)
    eclient = session.client('ec2')
    for key in mandatoryTags:
        if getattr(args, key) is not None:
            update_mandatory_ec2_tags(eclient, region, args)
            break
    if args.listEC2instances:
        list_ec2_instances(eclient, region, args.instances)
