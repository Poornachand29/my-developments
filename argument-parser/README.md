Tools
UpdateMandatoryTags
To be able to use the script, first you have to authenticate yourself in AWS (there must exist valid AWS credentials).

If parameter profile is not provided then by default default profile is used.

If parameter region is not provided then by default - script will be applied for both: us-east-1 and us-eat-2 regions

If at least one mandatory tag parameter is provided, then all of them must be provided.

If the list of mandatory tags changed, then in the code of the script it's easy to change the list of mandatory tags by modifying mandatory tags directory

Usage:
    update_ec2_mandatory_tags.py [-h] -i instance [instance ...]
                                    [-r region [region ...]] [-p profile] [-l]
                                    [-a tag_value] [-d tag_value]
                                    [-c tag_value] [-s tag_value]
                                    [-n tag_value] [-v tag_value]
                                    [-t tag_value]

    Scripts supports updating EC2 mandatory tags

    optional arguments:
    -h, --help            show this help message and exit
    -i instance [instance ...], --instances instance [instance ...]
                            "all" as the only value to this option if you wish to
                            update mandatory tags in all EC2 instances in
                            specified region or list of EC2 instances in which you
                            wishto update mandatory tags
    -r region [region ...], --regions region [region ...]
                            AWS regions
    -p profile, --profile profile
                            AWS profile name
    -l, --listEC2instances
                            List all EC2 instance IDs and their tags in the AWS
                            account and region
    -a tag_value, --application_id tag_value
                            Mandatory tag application_id
    -d tag_value, --app_inst_id tag_value
                            Mandatory tag app_inst_id
    -c tag_value, --chargeback_profile_id tag_value
                            Mandatory tag chargeback_profile_id
    -s tag_value, --cloud_support_group tag_value
                            Mandatory tag cloud_support_group
    -n tag_value, --os_name tag_value
                            Mandatory tag os_name
    -v tag_value, --os_version tag_value
                            Mandatory tag os_version
Examples:
Update 1 selected instance
        update_ec2_mandatory_tags.py -r us-east-1 -i i-019e7675c6bd17268 -r us-east-1 -a 169643 -d APL0000074470 -c 0000 -s "CTI GL CIS PUBLIC CLOUD" -n RedHat -v "7.9" -t "2021Q2" 
        Region: us-east-1
        Updated mandatory tags in i-019e7675c6bd17268 
List all EC2 instances in region us-east-1 in an account
      update_ec2_mandatory_tags.py -r us-east-1 -i all -r us-east-1 -l 
                                                                                                      
        List of EC2 instances in region: us-east-1
        i-07e735a8efe868560, AMI: ami-027f1abb2ed9a6508, application_id: Null, app_inst_id: Null, chargeback_profile_id: Null, cloud_support_group: Null, os_name: RedHatv1, os_version: Null, os_patchlevel: 2021Q2, appinstid: APL0000074471, Name: test-5/13

        AMI images used by the listed above EC2 instances:
        AmiID: ami-027f1abb2ed9a6508, Name: citi-rhel7.9-base-2021b_20210415-20210416-1618589431, OwnerId: 121938986777, Creation Date: 2021-04-16T16:31:42.000Z, Description: citi rhel7.9 base AMI
