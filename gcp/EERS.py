import os
from pprint import pprint
# Imports the Google Cloud Client Library.
from google.oauth2 import service_account
import googleapiclient.discovery

# Imports the Google Cloud Spanner Client Library.
from google.cloud import spanner
from google.cloud.spanner_v1 import param_types

# Get credentials
credentials = service_account.Credentials.from_service_account_file("c:/Users/poorn/Downloads/lateral-insight-271906-3b3b75e2859b.json")

# Create the Cloud IAM service object for roles
service = googleapiclient.discovery.build(
    'iam', 'v1', credentials=credentials)



# Create the Cloud IAM service object for policies
service1 = googleapiclient.discovery.build(
    'cloudresourcemanager', 'v1', credentials=credentials)

# Call the Cloud IAM Roles API
# If using pylint, disable weak-typing warnings
# pylint: disable=no-member
roles = service.roles().list(parent='projects/' + 'lateral-insight-271906').execute()['roles']

# Call the Cloud IAM policy API - Enable Cloud resource manager API
request = service1.projects().getIamPolicy(resource='lateral-insight-271906', body={"options": {"requestedPolicyVersion": 1}})
response = request.execute()
#print(response)

#Spanner connections
# Your Cloud Spanner instance ID.
instance_id = 'test'
# Your Cloud Spanner database ID.
database_id = 'test'

"""Queries sample data from the database using SQL."""
spanner_client = spanner.Client()
instance = spanner_client.instance(instance_id)
database = instance.database(database_id)

# Process the response
for role in roles:
    print('Title: ' + role['title'])
    print('Name: ' + role['name'])
    if 'description' in role :
        print('Description: ' + role['description'])
    print('')
    #Print Role permissions
    rolepermission = service.projects().roles().get(name=role['name']).execute()
    Permissions = []
    for permission in rolepermission['includedPermissions']:
        #print(permission)
        Permissions.append(permission)
    print(Permissions)
	#Print the members associated with the custom role
    for policy in response['bindings']:
        if ( policy['role'] ==  role['name'] ):
            print('members: ')
            print(policy['members'])
            memberid = policy['members']
            usercheck = 'user'
            Servicecheck = 'serviceAccount'
            user_res = [i for i in memberid if usercheck in i]
            Service_res = [i for i in memberid if Servicecheck in i]
            print(user_res)
            print(Service_res)
            if ( ('user' in str(user_res)) and ('serviceAccount' not in str(Service_res) )):
                memberid = 'user'
            elif (  ('serviceAccount' in str(Service_res)) and ('user' not in str(user_res)) ):
                memberid = 'serviceAccount'
            else:
                memberid = ['user' , 'serviceAccount' ] 
            #if (memberid[2] == 'u'):
            #    memberid = memberid[2:6]
            #    print(memberid)
            #elif (memberid[2] == 's'):
            #    memberid = memberid[2:16]
            #    print(memberid)
            print('role: ')
            print(policy['role'])
            print(' ')
    columnlist = []
	#Query the database information
    with database.snapshot() as snapshot:
     results = snapshot.execute_sql(
        'SELECT role_id FROM testtable')
	#appending the column name( Role name) in the list
    for row in results:
        columnname = u'{}'.format(*row)
        columnlist.append(columnname)
        print(columnlist)
	#Update the table if the role is not present in the column
    if role['name'] not in columnlist:
            #with database.batch() as batch:
            #    batch.insert(
            #        table='testtable',
            #        columns=('Name', 'owner', 'Permissions',),
            #        values=[(role['title'], policy['members'], role['description'])])
       #rolename = role['title']
       #memberid = str(policy['members'])
       #role_description = role['description']
       #print("type:")
       #print(type(rolename))
       #print(type(memberid))
       #print(type(role_description))
       role_id = role['name']
       is_privileged = False
       permissions_granted = str(Permissions)
       role_assignment_type = str(memberid)
       role_description = role['description']
       role_name = role['title']
       role_owner = "Rogerio"
       data_values = ( role_id, is_privileged, permissions_granted , role_assignment_type , role_description , role_name , role_owner  )
       data_type = param_types.Struct([
             param_types.StructField('roleid', param_types.STRING),
             param_types.StructField('is_privileged', param_types.BOOL),
             param_types.StructField('permissions_granted', param_types.STRING),
			 param_types.StructField('role_assignment_type', param_types.STRING),
			 param_types.StructField('role_description', param_types.STRING),
			 param_types.StructField('role_name', param_types.STRING),
			 param_types.StructField('role_owner', param_types.STRING),
       ])
       def insert_table(transaction):
           row_ct = transaction.execute_update(
               "INSERT testtable (role_id, is_privileged, permissions_granted, role_assignment_type, role_description, role_name, role_owner) VALUES "
               "(@values.roleid, @values.is_privileged, @values.permissions_granted, @values.role_assignment_type, @values.role_description, @values.role_name, @values.role_owner)",
                        params={'values' : data_values},
                        param_types={'values' : data_type}

           )

           print("{} record(s) inserted.".format(row_ct))

       database.run_in_transaction(insert_table)


#https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/iam/api-client/custom_roles.py

#export GOOGLE_APPLICATION_CREDENTIALS="/c/Users/poorn/Downloads/lateral-insight-test-320.json"

##Requirements:
#Spanner:
#pip install google-cloud-spanner
#pip install --upgrade google-api-python-client
#pip install --upgrade google-api-python-client google-auth google-auth-httplib2
#https://cloud.google.com/compute/docs/tutorials/python-guide
#IAM:
#https://cloud.google.com/iam/docs/quickstart-client-libraries
#Authentication:
##https://cloud.google.com/docs/authentication/production?_ga=2.129261071.2039032234.1584852332-484059598.1575601454
