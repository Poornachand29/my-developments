def minion_game(string):
    vowel_string = string
    consonant_string = string
    string_vowel_list = []
    consonant_vowel_list = []
    while len(vowel_string) >= 1:
        vowel_string = vow_func(vowel_string)
        for i in range(len(vowel_string)):
            string_vowel_list.append(vowel_string[:i+1])
        vowel_string = vowel_string[1:]
        #print(string_vowel_list)
    kevin_score = len(string_vowel_list)
                
    while len(consonant_string) >= 1:
        consonant_string = consonant_func(consonant_string)
        for i in range(len(consonant_string)):
            consonant_vowel_list.append(consonant_string[:i+1])
        consonant_string = consonant_string[1:]
        #print(consonant_vowel_list)
    stuart_score = len(consonant_vowel_list)
    
    if ( stuart_score > kevin_score):
        print(f"Stuart {stuart_score}")
    elif ( stuart_score < kevin_score):
        print(f"Kevin {kevin_score}")
    else:
        print("Draw")


def vow_func(vowel_string):
    vowels = ["A", "E", "I", "O", "U"]
    for i in range(len(vowel_string)):
        if vowel_string[i] in vowels:
            vowel_string_stripped = vowel_string[i:]
            break
        else:
            vowel_string_stripped = ""

    #print(vowel_string_stripped)
    return vowel_string_stripped

def consonant_func(consonant_string):
    vowels = ["A", "E", "I", "O", "U"]
    for i in range(len(consonant_string)):
        if consonant_string[i] not in vowels:
            consonant_string_stripped = consonant_string[i:]
            break
        else:
            consonant_string_stripped = ""

    #print(consonant_string_stripped)
    return consonant_string_stripped


if __name__ == '__main__':
    s = input()
    minion_game(s)
