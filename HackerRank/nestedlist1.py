if __name__ == '__main__':
    name_list = []
    score_list = []
    for _ in range(int(input())):
        name = input()
        score = float(input())
        name_list.append(name)
        score_list.append(score)
    
    lowest = min(score_list)
    lowest_indexes = [i for i, x in enumerate(score_list) if x == lowest]
    for i in lowest_indexes:
        name_list.pop(i)
    for i in lowest_indexes:
        score_list.pop(i)
    second_lowest = min(score_list)
    lowest_indexes = [i for i, x in enumerate(score_list) if x == second_lowest]
    final_list = []
    for o in lowest_indexes:
        final_list.append(name_list[o])
    final_list = sorted(final_list)
    for value in final_list:
        print(value)
