import re

if __name__ == "__main__":
    num1 = int(input())
    for i in range(num1):
        num2 = input().strip()
        if (re.search(r'^([7|8|9])(\d+)',str(num2))) and (len(num2) == 10) :
            print("YES")
        else:
            print("NO")
