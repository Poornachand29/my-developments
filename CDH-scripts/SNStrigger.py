import boto3
import os
import json
region = 'us-east-1'

def lambda_handler(event, context):
    ec2 = boto3.client('ec2', region_name=region)
    ec2name = boto3.resource('ec2')
    sns = boto3.client('sns')
    instances = os.environ['INSTANCES_ID']
    mdm_instances = instances.split(',')
    for instances_id in mdm_instances:
        response = ec2.describe_instances(InstanceIds=[instances_id])
        print(response)
        print(type(response))
        a = response["Reservations"][0]["Instances"][0]["State"]["Name"]
        b = response["Reservations"][0]["Instances"][0]["State"]["Code"]
        #print(response["ResponseMetadata"]["HTTPStatusCode"])
        #print(response["StoppingInstances"][0]["CurrentState"]["Name"])
        #print 'stopped your instances: '
        print(a)
        print(b)
        
        ec2instance = ec2name.Instance(instances_id)
        instancename = ''
        for tags in ec2instance.tags:
            if tags["Key"] == 'Name':
                instancename = tags["Value"]
                print(instancename)
    
        if  b == 80 and a == "stopped" :
            response1 = sns.publish(
            TopicArn='arn:aws:sns:us-east-1:178802520617:topic',    
            Message='Your instance is stopped' + str(instancename),
            Subject='Instance state'
        )
        else:
            response2 = sns.publish(
            TopicArn='arn:aws:sns:us-east-1:178802520617:topic',    
            Message='Please check the state of your instance'  + str(instancename),
            Subject='Instance state'
        )    
    