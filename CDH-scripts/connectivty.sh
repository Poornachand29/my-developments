# --------------------------------------------------------------------------------------#
# Script Name           : Connectivity.sh
# Author                : Systech solutions 
# Date                  : 24/06/2019
# Description          : This Script will check the connectivity between the running server to the Database port
# Usage                : This checks the connectivity to ensure the load status
# Modification Log      : 
# Date                  : 
# Modified By           : 
# Description           : 
# --------------------------------------------------------------------------------------#
#!/bin/sh
host=www.google.com
DATE=`date +%Y-%m-%d`
TIME=`date +%H%M%S`
LOG_OK=/tmp/telnet_ok
LOG_FAIL=/tmp/telnet_falha

#the port to check the connections
for port in 80
do
#Telnet to the port and checking the status 
if telnet -c $host $port </dev/null 2>&1 | grep -q Connected; then
  echo "$DATE $TIME  $port: Connected" >> $LOG_OK
#Triggering an email about the connection status  
  echo "Server is in sync with Database" | mail -s "Server connection status" poornachands@systechusa.com
else
  echo "$DATE $TIME $port : no connection" >> $LOG_FAIL
#Triggering an email about the connection status  
  echo "Server is not in sync with Database" | mail -s "Server connection status" poornachands@systechusa.com
fi
done
