#----------------------------------------------#
# Script name: CDH_stop_Dev_QA_function        #
# Author : Systech Solutions                   #
# Date : 14/12/18                              #
# Description : Stop the Dev and QA instances  #
#               for informatica                #
# Modified log:                                #
# Date: 14/12/18                               #
# Modified by:                                 #     
# Description:                                 #
#----------------------------------------------#
import boto3
import os
# Region of the QA instances
region = 'us-west-2'
#Dev and QA instances CDHDEVSA1,CDHDEVSA2,CDHQASA1,CDHQASA2
#instances = ['i-02cbc942f83b49fe8','i-02c27f10d91842a46','i-09f6eabaddae6bfa3','i-0103692f1e0d0218d']


def lambda_handler(event, context):
    ec2 = boto3.client('ec2', region_name=region)
    instances = os.environ['INSTANCES_ID']
    informatica_instances = instances.split(',')
    print(informatica_instances)
    for instances_id in informatica_instances:
        response = ec2.stop_instances(InstanceIds=[instances_id])
        print(response)
        print 'stopped your instances: ' + str(instances_id)