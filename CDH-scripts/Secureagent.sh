# --------------------------------------------------------------------------------------#
# Script Name           : Secureagent.sh
# Author                : Systech solutions 
# Date                  : 20/12/2018
# Description          : This Script will start the Informatica secure agent once the instance is up
# Usage                : This will automate the manual process starting up the agent every time when the 
#                        agent is up 
# Modification Log      : 
# Date                  : 
# Modified By           : 
# Description           : 
# --------------------------------------------------------------------------------------#
#!/bin/sh

#Log location to server
LOG_LOCATION=/home/cdh_infa_user/logs
exec >> $LOG_LOCATION/secureagent_$(date +"%Y-%m-%d").log

sleep 10

v_Infa_Path=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/agentcore
v_loop=0;
User_emails="CDHProdSupport@PacificLife.com"

cd $v_Infa_Path

#Secure Agent status
v_Serivice_Status=`./consoleAgentManager.sh getstatus`

echo "Current agent status"
echo $v_Serivice_Status

#Check whether return is READY
v_service_flag=`echo $v_Serivice_Status|awk '{print match($0,"READY")}'`

echo "Flag status"
echo $v_service_flag

#checking the status of agent
if [ $v_service_flag -gt 0 ];then
echo "Agent already started" $(date +"%Y-%m-%d %H:%M:%S")
exit 0;
else
v_Infa_Start=`./infaagent startup`
echo "Agent started up at" $(date +"%Y-%m-%d %H:%M:%S")
fi

sleep 5

#Checking the Agent status for a count of 11 with delay of 3 seconds

while [ $v_loop -lt 21 ]
do
echo $v_loop "Checking the status" $(date +"%Y-%m-%d %H:%M:%S")
v_Serivice_Status=`./consoleAgentManager.sh getstatus`
v_service_flag=`echo $v_Serivice_Status|awk '{print match($0,"READY")}'`
sleep 3
if [ $v_service_flag -eq 0  -a  $v_loop -gt 19 ];then
echo "couldn't able to start the agent"
echo "couldn't able to start the Secureagent" | mail -s "Secure agent status" $User_emails
elif [ $v_service_flag  -gt  0 ]; then
echo agent started up successfully at $(date +"%Y-%m-%d %H:%M:%S")
exit 0;
fi
v_loop=`expr $v_loop + 1`;
done

exit 0;

