import boto3
import os
from datetime import datetime, timezone
#from django.utils import timezone
import datetime
import dateutil
iam_client = boto3.client('iam')

def lambda_handler(event, context):
    #users = os.environ['user_names']
    users = ['user2','User1']
    #users = [users]
    print(type(users))
    for user in users:
        inactive_keys = 0
        active_keys = 0
        keys = iam_client.list_access_keys(UserName=user)
        for key in keys['AccessKeyMetadata']:
            print(key['Status'])
            createddate = key['CreateDate']
            print(createddate)
            #createddate=2018-12-26 06:00:54+00:00
            #now_aware = timezone.now()
            #print(now_aware)
            todaysdate = datetime.datetime.now(timezone.utc)
            print(todaysdate)
            timelimit=datetime.datetime.now(timezone.utc) - datetime.timedelta(days=1)
            #timelimit = todaysdate - createddate
            print(timelimit)
            #timelimitc = timedelta(timelimit)
            #print(timelimitc)
            if key['Status']=='Inactive': inactive_keys = inactive_keys + 1
            elif key['Status']=='Active': active_keys = active_keys + 1
            #print(key['Created'])
        print("%s has %d inactive keys and %d active keys" % (user, inactive_keys, active_keys))
        if inactive_keys + active_keys >= 2:
            print ("%s has 2 keys. You must delete a key before you can create another key" % (user)) 
            create_key = "false"
        else:
            create_key = "true"
        print(create_key)
        print(timelimit)
        print(createddate)
        if (create_key == "true") and (timelimit >= createddate):
            access_key_metadata = iam_client.create_access_key(UserName=user)['AccessKey']
            access_key = access_key_metadata['AccessKeyId']
            secret_key = access_key_metadata['SecretAccessKey']
            print(access_key,secret_key)
            