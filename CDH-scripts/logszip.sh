# --------------------------------------------------------------------------------------#
# Script Name           : logscopy.sh
# Author                : Systech solutions 
# Date                  : 20/12/2018
# Description          : This Script will zip the logs,clean up the logs and move the logs to S3 bucket
# Usage                : This will remove the zip logs which are occupying lots of space 
# Modification Log      : 
# Date                  : 
# Modified By           : 
# Description           : 
# --------------------------------------------------------------------------------------#
#!/bin/bash

#Log location to server
LOG_LOCATION=/home/cdh_infa_user/logs
exec >> $LOG_LOCATION/logfiles_$(date +"%Y-%m-%d").log

path[1]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/logs/
path[2]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/data/logs/
path[3]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/data/success/
path[4]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/data/temp/
path[5]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/data/error/
path[6]=/home/cdh_infa_user/data/pladmin/MyLinuxAgent/apps/Data_Integration_Server/data/cache/

bucket=s3://pl-cdh-qa1-logging

r=1
for i in "${path[@]}"
do
echo $i
zip -r "logsarchive$r-$(date +"%Y-%m-%d").zip" $i

aws s3 mv logsarchive$r-$(date +"%Y-%m-%d").zip $bucket

rm -r $i*

r=`expr $r + 1`
done
exit 0;