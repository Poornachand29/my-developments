import os
import psycopg2
import psycopg2.extras
import sys
import boto3
import json
from datetime import datetime


def lambda_handler(event, context):
  #Redshift database connection details	
  parameter_group = os.environ['parameter_group_name']
  REDSHIFT_DATABASE = os.environ['REDSHIFT_DATABASE']
  REDSHIFT_USER = os.environ['REDSHIFT_USER']
  REDSHIFT_PASSWD = os.environ['REDSHIFT_PASSWD']
  REDSHIFT_PORT = os.environ['REDSHIFT_PORT']
  REDSHIFT_ENDPOINT = os.environ['REDSHIFT_ENDPOINT']
  REDSHIFT_QUERY = " select * from edw.etl_load_control where job_name = 'mct_m_EDW_PUBLISH_TO_ODSEXT_FIN_PROF_SEGMENT' "
  
  #creating a connection to the redshift database

  try:
    conn = psycopg2.connect(
      dbname=REDSHIFT_DATABASE,
      user=REDSHIFT_USER,
      password=REDSHIFT_PASSWD,
      port=REDSHIFT_PORT,
      host=REDSHIFT_ENDPOINT)
  except Exception as ERROR:
    print("Execution Issue: " + str(ERROR))
    sys.exit(1)

  try:
	#creating a cursor to execute the Redshift query 
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    print(cursor.execute(REDSHIFT_QUERY))
	#print the column names
    names = [description[0] for description in cursor.description]
    print names
    r = [dict((cursor.description[i][0], value) \
              for i, value in enumerate(row)) for row in cursor.fetchall()]
	#extracting a string representing a json object from an object
    json_dumps = json.dumps(r, indent=4, sort_keys=True, default=str)
    #json_dumps = json.dumps(r)
    print(json_dumps)
	#extracting an object from a string representing a json object
    json_loads = json.loads(json_dumps)
    print(json_loads)
	#Printing a particular object from the json objects
    load_end_date = json_loads[0]['job_end_datetime']
    print(load_end_date)
    cursor.close()
    conn.commit()
    conn.close()
  except Exception as ERROR:
    print("Execution Issue: " + str(ERROR))
    sys.exit(1)
  #fetching todays date
  todays_date = datetime.now()
  print(todays_date)
  converted_todays_date = todays_date.strftime("%Y-%m-%d %H:%M:%S")
  print(converted_todays_date)
  
  #To check job status 
  load_stop_YMD = load_end_date.strftime("%Y-%m-%d %H:%M:%S")
  todays_date_YMD = todays_date.strftime("%Y-%m-%d %H:%M:%S")
  if load_stop_YMD ==   todays_date_YMD :
	print("Load Ended")
  else :
    print("Please check the load")
  	  
  #Setting the WLM    
  if converted_todays_date >= load_end_date and load_stop_YMD == todays_date_YMD :
	    response = client.modify_cluster_parameter_group(
        ParameterGroupName=parameter_group,
        Parameters=[
			{
			 'ParameterName': 'wlm_json_configuration',
			  'ParameterValue': '[{"query_concurrency" : 25,"memory_percent_to_use" : 80,"query_group" : [ ],"query_group_wild_card" : 0,"user_group" : [ "cdh_etl" ],"user_group_wild_card" : 0, "concurrency_scaling" : "auto"},{"query_concurrency" : 10,"memory_percent_to_use" : 10,"query_group" : [ ],"query_group_wild_card" : 0,"user_group" : [ "cdh_support" ],"user_group_wild_card" : 0,"concurrency_scaling" : "auto"},{"query_concurrency" : 5,"memory_percent_to_use" : 5,"query_group" : [ ],"query_group_wild_card" : 0,"user_group" : [ "rsd_di","rsd_bi" ],"user_group_wild_card" : 0, "concurrency_scaling" : "auto"},{"query_concurrency" : 5,"memory_percent_to_use" : 5,"query_group" : [ ],"query_group_wild_card" : 0,"user_group" : [ ],"user_group_wild_card" : 0,"concurrency_scaling" : "auto"},{"short_query_queue" : true}]',
			  'Description': 'Custom parameter group',
			  'DataType': 'string',
			  'ApplyType': 'dynamic',
			 'IsModifiable': True
	        }
		   ]
		)
        # Providing higher concurrency and memory to Load user at Morning
  else :
		print("no change in parameter group")